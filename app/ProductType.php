<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = [
        'name',
        'image_path',
    ];

    public function product()
    {
      return $this->hasMany(Product::class);
    }
}
