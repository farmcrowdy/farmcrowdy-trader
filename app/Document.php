<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'source_id',
        'source_type',
        'file_path',
    ];
}
