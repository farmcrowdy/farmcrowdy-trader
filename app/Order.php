<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $casts = ['delivery_date' => 'date'];
    protected $fillable = [
        'user_id',
        'product_id',
        'status',
        'color',
        'hardness',
        'size',
        'dryness_process',
        'foreign_matter',
        'moisture',
        'weevil_process',
        'broken_material',
        'rotten',
        'damaged_kernel',
        'splits',
        'test_weight',
        'total_defects',
        'oil_content',
        'dockage',
        'infestations',
        'volatile',
        'hectoliter_weight',
        'mammalican',
        'ash_content',
        'curcumin_content',
        'acid_insoluble_ash',
        'insect_defiled',
        'mold_by_weight',
        'extraneous_by_weight',
        'whole_dead_insects',
        'file_path',
        'quantity',
        'unit_id',
        'demand_price',
        'delivery_date',
        'currency_id',
        'delivery_country_id',
        'delivery_address',
        'agree_info',
        'agree_intention',
        'agree_feedback',
        'agree_penalty',
        'additional_information'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'delivery_country_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    /**
     * Get the Order's messages.
     */
    public function messages()
    {
        return $this->morphMany('App\Message', 'messageable');
    }
}
