<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'name',
        'symbol'
    ];

    public function offer()
    {
      return $this->hasMany(Offer::class);
    }

    public function order()
    {
      return $this->hasMany(Offer::class);
    }
}
