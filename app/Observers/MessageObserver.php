<?php

namespace App\Observers;
use Illuminate\Support\Facades\Auth;

use App\Message;
use Illuminate\Support\Facades\Mail;
use App\Mail\MessageMail;


class MessageObserver
{
    /**
     * Handle the message "created" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
     public function creating(Message $message)
     {
       $user = Auth::user();
       if ($user->isAdmin()) {
         $message->sender = 0;
         //$message->receiver = $message->messageable->user_id;
       }
     }

    public function created(Message $message)
    {
        Mail::to('fctraderadmins@farmcrowdy.com')->send(new MessageMail($message));
    }

    /**
     * Handle the message "updated" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function updated(Message $message)
    {
        //
    }

    /**
     * Handle the message "deleted" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function deleted(Message $message)
    {
        //
    }

    /**
     * Handle the message "restored" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function restored(Message $message)
    {
        //
    }

    /**
     * Handle the message "force deleted" event.
     *
     * @param  \App\Message  $message
     * @return void
     */
    public function forceDeleted(Message $message)
    {
        //
    }
}
