<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'sortname',
        'name',
        'phonecode',
        'flag_path'
    ];

    public function offer()
    {
      return $this->hasMany(Offer::class);
    }
}
