<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Order;
use Illuminate\Support\Facades\Auth;


class CloneOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order = Order::findOrFail($this->id);
        return Auth::user()->id == $order->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function clone()
    {
        $order = Order::findOrFail($this->id);
        $clonedOrder = $order->replicate();
        $clonedOrder->status = "pending";
        $clonedOrder->save();
        $order_file_path = str_replace('public/','storage/', $order->file_path);
        $order_file_sections = explode('/', $order_file_path);
        $order_file_name = explode('.', $order_file_sections[2]);
        $copy_file_name = $order_file_name[0].'_'.$clonedOrder->id;
        $new_file_path = "storage/order_images/".$copy_file_name.'.'.$order_file_name[1];

        if(\File::exists($order_file_path)){
            $result = \File::copy($order_file_path, $new_file_path);
        }
    
        $clonedOrder->file_path = $new_file_path;
        $clonedOrder->save();
    }
}