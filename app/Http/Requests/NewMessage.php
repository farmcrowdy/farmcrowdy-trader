<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Rules\MessageableTypes;
use App\Message;
use App\Offer;
use App\Order;

class NewMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->id == Auth::user()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $checks = [
            'sender' => 'required|integer',
            'receiver' => 'required|integer',
            'body' => 'required',
            'messageable_type' => ['required', new MessageableTypes,],
            'messageable_id' => 'required|integer',
        ];

        if($this->hasFile('file_path')){
            $checks['file_path'] = 'required|image|mimes:jpeg,png,jpg,gif|max:1024';
        }
        return $checks;
    }

    public function store()
    {
        $valid_data = $this->validated();
        $valid_data['messageable_type'] = $this->get_messageable_type_class($this->messageable_type, $this->messageable_id);
        $valid_data['file_path'] = $this->uploadFile('file_path', 'messages');
        $newMessage = Message::create($valid_data);
        $newMessage->save();
        return $newMessage;
    }

    private function uploadFile($file_parameter, $destinationFolder)
    {
        if($this->hasFile($file_parameter)){

            $filenameoriginal = str_replace(' ', '', $this->file($file_parameter)->getClientOriginalName());
            $filename = pathinfo($filenameoriginal, PATHINFO_FILENAME);
            $extension = $this->file($file_parameter)->getClientOriginalExtension();
            $destinationPath = 'public/'.$destinationFolder;

            
            //create new $filename
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            $db_path = $destinationFolder.'/'.$filenameToStore;
            
            // upload image
            $this->file($file_parameter)->storeAs($destinationPath,$filenameToStore);
            return $db_path; 
        }else{
            return false;
        }
    }

    private function get_messageable_type_class($messageable_type, $messageable_id)
    {
        switch ($messageable_type) {
            case 'Order':
                return (!empty(Order::findOrFail($messageable_id))) ? Order::class : false;
                break;

            case 'Offer':
                return (!empty(Offer::findOrFail($messageable_id))) ? Offer::class : false;
                break;

            default:
                fail('Could not get valid messageable type.');
                break;
        }
    }
}
