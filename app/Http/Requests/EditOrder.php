<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\Order;


class EditOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->id == Auth::user()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'quantity' => 'required',
            'unit_id' => 'required',
            'demand_price' => 'required',
            'delivery_date' => 'required',
            'currency_id' => 'required',
            'delivery_country_id' => 'required',
            'delivery_address' => 'required',
            'foreign_matter' => 'nullable',
            'moisture' => 'nullable',
            'color' => 'required',
            'hardness' => 'required',
            'dryness_process' => 'required',
            'size' => 'required',
            'weevil_process' => 'nullable',
            'broken_material' => 'nullable',
            'rotten' => 'nullable',
            'damaged_kernel' => 'nullable',
            'splits' => 'nullable',
            'test_weight' => 'nullable',
            'total_defects' => 'nullable',
            'oil_content' => 'nullable',
            'dockage' => 'nullable',
            'infestations' => 'nullable',
            'volatile' => 'nullable',
            'hectoliter_weight' => 'nullable',
            'mammalican' => 'nullable',
            'ash_content' => 'nullable',
            'curcumin_content' => 'nullable',
            'acid_insoluble_ash' => 'nullable',
            'insect_defiled' => 'nullable',
            'mold_by_weight' => 'nullable',
            'extraneous_by_weight' => 'nullable',
            'whole_dead_insects' => 'nullable',
            'file_path' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'agree_info' => 'required',
            'agree_intention' => 'required',
            'agree_feedback' => 'required',
            'agree_penalty' => 'required',
            'additional_information' => 'present|nullable',
        ];
    }

    public function save()
    {
        $order = Order::findOrFail($this->order_id);
        $valid_data = $this->validated();
        $valid_data['file_path'] = $this->uploadFile($order, 'file_path', 'order_images');
        $valid_data['user_id'] = Auth::user()->id;
        Order::where('id', $order->id)->update($valid_data);
    }

    private function uploadFile($order, $file_parameter, $destinationFolder)
    {
        if($this->hasFile($file_parameter)){

            $filenameoriginal = trim($this->file($file_parameter)->getClientOriginalName());
            $filename = pathinfo($filenameoriginal, PATHINFO_FILENAME);
            $extension = $this->file($file_parameter)->getClientOriginalExtension();
            $destinationPath = 'public/'.$destinationFolder;

            //create new $filename
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Delete old image file
            $order_file_path = str_replace('public/','storage/', $order->file_path);
            if(\File::exists($order_file_path)){
                \File::delete($order_file_path);
            }
            // upload new image
            return $this->file($file_parameter)->storeAs($destinationPath,$filenameToStore);
        }else{
            return $order->file_path;
        }
    }
}
