<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Order;
use Illuminate\Support\Facades\Auth;


class DeleteOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order = Order::findOrFail($this->id);
        return Auth::user()->id == $order->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function delete()
    {
        $order = Order::findOrFail($this->id);
        // Remove the image file
        $order_file_path = str_replace('public/','storage/', $order->file_path);
        if(\File::exists($order_file_path)){
            \File::delete($order_file_path);
        }
        $order->delete();
    }
}
