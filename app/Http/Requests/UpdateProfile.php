<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->id == Auth::user()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'product_id' => 'required',
            'country_id' => 'required|integer',
            'profile_image_url' => 'image|mimes:jpeg,png,jpg,gif|max:1024',
            'old_password' => 'required_with:new_password',
            'new_password' => 'required_with:old_password',
            'confirm_new_password' => 'same:new_password'
        ];
    }

    public function update()
    {
        $valid_data = $this->validated();
        $user = User::findOrFail($this->user()->id);
        $valid_data['profile_image_url'] = $this->uploadFile($user, 'profile_image_url', 'profile_images');
        if(!empty($this->new_password)){
            $valid_data['password'] = Hash::make($this->new_password);
        }
        unset($valid_data['old_password']);
        unset($valid_data['new_password']);
        unset($valid_data['confirm_new_password']);
        User::where('id', $user->id)->update($valid_data);
    }

    private function uploadFile($user, $file_parameter, $destinationFolder)
    {
        if($this->hasFile($file_parameter)){

            $filenameoriginal = trim($this->file($file_parameter)->getClientOriginalName());
            $filename = pathinfo($filenameoriginal, PATHINFO_FILENAME);
            $extension = $this->file($file_parameter)->getClientOriginalExtension();
            $destinationPath = 'public/'.$destinationFolder;
            
            //create new $filename
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Delete old image file
            $user_profile_image_url = str_replace('public/','storage/', $user->profile_image_url);
            if(\File::exists($user_profile_image_url)){
                \File::delete($user_profile_image_url);
            }
            // upload new image
            return $this->file($file_parameter)->storeAs($destinationPath,$filenameToStore);
        }else{
            return $user->profile_image_url;
        }
    }
}
