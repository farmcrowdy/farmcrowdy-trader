<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Offer;
use Illuminate\Support\Facades\Auth;


class CloneOffer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $offer = Offer::findOrFail($this->id);
        return Auth::user()->id == $offer->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function clone()
    {
        $offer = Offer::findOrFail($this->id);
        $clonedOffer = $offer->replicate();
        $clonedOffer->status = "pending";
        $clonedOffer->save();
        $offer_file_path = str_replace('public/','storage/', $offer->file_path);
        $offer_file_sections = explode('/', $offer_file_path);
        $offer_file_name = explode('.', $offer_file_sections[2]);
        $copy_file_name = $offer_file_name[0].'_'.$clonedOffer->id;
        $new_file_path = "storage/offer_images/".$copy_file_name.'.'.$offer_file_name[1];

        if(\File::exists($offer_file_path)){
            $result = \File::copy($offer_file_path, $new_file_path);
        }
        $clonedOffer->file_path = $new_file_path;
        $clonedOffer->save();
    }
}
