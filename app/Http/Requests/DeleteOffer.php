<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Offer;
use Illuminate\Support\Facades\Auth;

class DeleteOffer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $offer = Offer::findOrFail($this->id);
        return Auth::user()->id == $offer->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function delete()
    {
        $offer = Offer::findOrFail($this->id);
        // Remove the image file
        $offer_file_path = str_replace('public/','storage/', $offer->file_path);
        if(\File::exists($offer_file_path)){
            \File::delete($offer_file_path);
        }
        $offer->delete();
    }
}
