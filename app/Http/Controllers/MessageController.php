<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewMessage;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Message;
use App\Order;
use App\Offer;


class MessageController extends Controller
{
    public function store(NewMessage $request)
    {
        $response = $request->store();
        $response->setHidden(['messageable_type', 'messageable_id', 'receiver', 'updated_at']);
        $sender = User::findOrFail($response->sender);
        $response->author = $sender->first_name . ' ' . $sender->last_name;
        $response->time_sent = $response->created_at->format('h:ma j/m/Y');
        $response->role = $sender->role_id;
        if (!empty($response->file_path)) {
            $response->image = url('/') . '/'.'storage/' .$response->file_path;
        }
        $response->content = (object)[
            'text' => $response->body,
            'image' => $response->image,
        ];
        
        return response()->json($response, 200);
    }

    public function previous_messages(Request $request)
    {
        $messages = Message::where([
            ['id', '<', $request->input('index')],
            [
                'messageable_type', '=',
                $this->get_messageable_type_class(
                    $request->input('messageable_type'),
                    $request->input('messageable_id')
                )
            ],
            ['messageable_id', '=', $request->input('messageable_id')],
        ])->where(function ($query) {
            $query->where([
                ['sender', '=', Auth::user()->id],
                ['receiver', '=', 0]
            ])
                ->orWhere([
                    ['sender', '=', 0],
                    ['receiver', '=', Auth::user()->id]
                ]);
        })
            ->latest()
            ->take($request->input('limit'))
            ->reorder('created_at', 'asc')
            ->get();

        $messages = $this->process_messages($messages);
        return response()->json($messages, 200);
    }

    public function recent_messages(Request $request)
    {
        $messages = Message::where([
            [
                'messageable_type', '=',
                $this->get_messageable_type_class(
                    $request->input('messageable_type'),
                    $request->input('messageable_id')
                )
            ],
            ['messageable_id', '=', $request->input('messageable_id')],
        ])->where(function ($query) {
            $query->where([
                ['sender', '=', Auth::user()->id],
                ['receiver', '=', 0]
            ])
                ->orWhere([
                    ['sender', '=', 0],
                    ['receiver', '=', Auth::user()->id]
                ]);
        })
        ->orderBy('id', 'desc')
        ->take($request->input('limit'))
        // ->latest()
        ->get();
        // dd($messages);
        $rev_messages = $messages->reverse();


        // $rev_messages = $this->process_messages($rev_messages);
        // return response()->json($rev_messages, 200);
        $messages = $this->process_messages($messages);
        return response()->json($messages, 200);
    }

    public function latest_messages(Request $request)
    {
        $messages = Message::where([
            ['id', '>', $request->input('last')],
            [
                'messageable_type', '=',
                $this->get_messageable_type_class(
                    $request->input('messageable_type'),
                    $request->input('messageable_id')
                )
            ],
            ['messageable_id', '=', $request->input('messageable_id')],
        ])->where(function ($query) {
            $query->where([
                ['sender', '=', Auth::user()->id],
                ['receiver', '=', 0]
            ])
                ->orWhere([
                    ['sender', '=', 0],
                    ['receiver', '=', Auth::user()->id]
                ]);
        })
            ->latest()
            ->take($request->input('limit'))
            ->reorder('created_at', 'asc')
            ->get();

        $messages = $this->process_messages($messages);
        return response()->json($messages, 200);
    }

    private function get_messageable_type_class($messageable_type, $messageable_id)
    {
        switch ($messageable_type) {
            case 'Order':
                return (!empty(Order::findOrFail($messageable_id))) ? Order::class : false;
                break;

            case 'Offer':
                return (!empty(Offer::findOrFail($messageable_id))) ? Offer::class : false;
                break;

            default:
                fail('Could not get valid messageable type.');
                break;
        }
    }

    private function process_messages($messages)
    {
        if (!empty($messages)) {
            foreach ($messages as $message) {
                $message->setHidden(['messageable_type', 'messageable_id', 'receiver', 'updated_at']);
                if ($message->sender == 0) {
                    $message->author = "Admin";
                    $message->role = 2;
                } else {
                    $message->author = Auth::user()->first_name . ' ' . Auth::user()->last_name;
                    $message->role = Auth::user()->role_id;
                }
                $message->time_sent = $message->created_at->format('h:ma j/m/Y');
                if (!empty($message->file_path)) {
                    $message->image = url('/storage') . '/' .$message->file_path;
                }
                $message->content = (object)[
                    'text' => $message->body,
                    'image' => $message->image,
                ];
            }
            return $messages;

        } else {
            return [];
        }
    }
}
