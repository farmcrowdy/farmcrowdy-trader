<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfile;

class UserProfileController extends Controller
{
    public function update(UpdateProfile $request)
    {
        $request->update();
        return redirect('profile/')->with('profile_updated', "Your Profile has been updated!");
    }
}
