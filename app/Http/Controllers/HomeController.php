<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Unit;
use App\Product;
use App\ProductType;
use App\Currency;
use App\Order;
use App\Offer;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'welcome']);
    }

    public function welcome()
    {

        if (Auth::check()) {
           return redirect('home');
        }else{
            $orders  = Order::where('status', 'ongoing')->get();
            $offers  = Offer::where('status', 'ongoing')->get();
            $products = collect($orders)->merge($offers)->sortByDesc('created_at')->take(12);
            // dd($products);
            $view_data = [
                'products' => $products,
            ];
            return view('welcome', $view_data);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders  = Order::where('status', 'ongoing')->get();
        $offers  = Offer::where('status', 'ongoing')->get();
        $products = collect($orders)->merge($offers)->sortByDesc('created_at')->paginate(24);
        $view_data = [
            'products' => $products,
        ];
        return view('fctrader.index', $view_data);
    }

    public function search_offer(Request $request)
    {
      $user = Auth::user();
      if($request->search_offer !== NULL){
        session(['search_offer' => $request->search_offer]);
      }
      $searchOffer = session('search_offer');
      $prod = Product::where('name', 'like', '%'.$searchOffer.'%')->first();

      $orders  = Order::where('status', 'ongoing')->where('product_id', $prod->id)->get();
      $offers  = Offer::where('status', 'ongoing')->where('product_id', $prod->id)->get();

      $products = collect($orders)->merge($offers)->sortByDesc('created_at')->paginate(24);

      $view_data = [
          'products' => $products,
      ];
      return view('fctrader.index', $view_data);
    }

    public function my_listings()
    {
        $user = Auth::user();
        $orders  = Order::where('user_id', $user->id)->get();
        $offers  = Offer::where('user_id', $user->id)->get();
        $products = collect($orders)->merge($offers)->sortByDesc('created_at')->paginate(24);
        $view_data = [
            'products' => $products,
        ];
        return view('fctrader.my_listings', $view_data);
    }

    public function search_product(Request $request)
    {
      $user = Auth::user();
      if($request->search_product !== NULL){
        session(['search_product' => $request->search_product]);
      }
      $searchProd = session('search_product');
      $prod = Product::where('name', 'like', '%'.$searchProd.'%')->first();

      $orders  = Order::where('user_id', $user->id)->where('product_id', $prod->id)->get();
      $offers  = Offer::where('user_id', $user->id)->where('product_id', $prod->id)->get();

      $products = collect($orders)->merge($offers)->sortByDesc('created_at')->paginate(24);

      $view_data = [
          'products' => $products,
      ];
      return view('fctrader.my_listings', $view_data);
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function orders()
    {
        $user = Auth::user();
        $orders = Order::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return view('orders', ['orders' => $orders]);
    }

    public function order_details($id)
    {
        $user = Auth::user();
        $user->full_name = $user->first_name .' '.$user->last_name;
        $order = Order::findOrFail($id);
        $view_data = [
            'user' => $user,
            'order' => $order
        ];
        return view('order_details', $view_data);
    }

    public function create_order()
    {
        $units = Unit::all();
        $products = Product::all();
        $produce_types = ProductType::all();
        $countries = Country::all();
        $currencies = Currency::all();
        $message = '';
        $view_data = [
            'units' => $units,
            'products' => $products,
            'produce_types' => $produce_types,
            'countries' => $countries,
            'currencies' => $currencies,
            'message' => $message
        ];

        return view('create_order', $view_data);
    }

    public function edit_order($id)
    {
        $order = Order::findOrFail($id);
        if($order){
            $units = Unit::all();
            $products = Product::all();
            $produce_types = ProductType::all();
            $countries = Country::all();
            $currencies = Currency::all();
            $message = '';
            $view_data = [
                'order' => $order,
                'units' => $units,
                'products' => $products,
                'produce_types' => $produce_types,
                'countries' => $countries,
                'currencies' => $currencies,
                'message' => $message
            ];

            return view('edit_order', $view_data);

        }
    }

    public function offers()
    {
        $user = Auth::user();
        $offers = Offer::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return view('offers', ['offers' => $offers]);
    }

    public function offer_details($id)
    {
        $user = Auth::user();
        $user->full_name = $user->first_name .' '.$user->last_name;
        $offer = Offer::findOrFail($id);
        $view_data = [
            'user' => $user,
            'offer' => $offer
        ];
        return view('offer_details', $view_data);
    }

    public function create_offer()
    {
        $units = Unit::all();
        $products = Product::all();
        $produce_types = ProductType::all();
        $countries = Country::all();
        $currencies = Currency::all();
        $message = '';
        $view_data = [
            'units' => $units,
            'products' => $products,
            'produce_types' => $produce_types,
            'countries' => $countries,
            'currencies' => $currencies,
            'message' => $message
        ];

        return view('create_offer', $view_data);
    }

    public function edit_offer($id)
    {
        $offer = Offer::findOrFail($id);
        if($offer){
            $units = Unit::all();
            $products = Product::all();
            $produce_types = ProductType::all();
            $countries = Country::all();
            $currencies = Currency::all();
            $message = '';
            $view_data = [
                'offer' => $offer,
                'units' => $units,
                'products' => $products,
                'produce_types' => $produce_types,
                'countries' => $countries,
                'currencies' => $currencies,
                'message' => $message
            ];

            return view('edit_offer', $view_data);

        }
    }

    public function profile()
    {
        $user = Auth::user();
        $products = Product::all();
        $countries = Country::all();
        $view_data = [
            'user' => $user,
            'products' => $products,
            'countries' => $countries,
        ];
        // dd($user);
        return view('profile', $view_data);
    }

    public function insights()
    {
        return view('insight_index');
    }
}
