<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewOrder;
use App\Http\Requests\EditOrder;
use App\Http\Requests\CloneOrder;
use App\Http\Requests\DeleteOrder;
use App\Order;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function create(NewOrder $request)
    {
        $request->save();
        return redirect('orders')->with('new_order', 'A New Order has been created!');
    }

    public function update(EditOrder $request)
    {
        $request->save();
        return redirect('orders')->with('update_order', 'Your Order has been updated!');
    }

    public function clone(CloneOrder $request, $id)
    {
        $request->clone();
        return redirect('orders')->with('clone_order', "You have cloned Order #$id successfully.");
    }

    public function delete(DeleteOrder $request, $id)
    {
        $request->delete();
        return redirect('orders')->with('delete_order', "Order #$id has been deleted!");
    }
}
