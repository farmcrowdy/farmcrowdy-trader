<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NewOffer;
use App\Http\Requests\EditOffer;
use App\Http\Requests\CloneOffer;
use App\Http\Requests\DeleteOffer;
use App\Offer;
use Illuminate\Support\Facades\Auth;

class OfferController extends Controller
{
    public function create(NewOffer $request)
    {
        $request->save();
        return redirect('offers')->with('new_offer', 'A New Offer has been created!');
    }

    public function update(EditOffer $request)
    {
        $request->save();
        return redirect('offers')->with('update_offer', 'Your Offer has been updated!');
    }
    
    public function clone(CloneOffer $request, $id)
    {
        $request->clone();
        return redirect('offers')->with('clone_offer', "You have cloned Offer #$id successfully.");
    }
    
    public function delete(DeleteOffer $request, $id)
    {
        $request->delete();
        return redirect('offers')->with('delete_offer', "Offer #$id has been deleted!");
    }
}
