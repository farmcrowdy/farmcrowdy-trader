<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\MorphMany;
use Laravel\Nova\Http\Requests\NovaRequest;

class Offer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Offer::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    //public static $title = 'product';
    public function title()
      {
          return $this->product->name;
      }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('User')->sortable(),
            BelongsTo::make('Product')->sortable(),
            BelongsTo::make('Unit')->sortable(),
            BelongsTo::make('Currency')->sortable(),
            BelongsTo::make('Source Country', 'country', 'App\Nova\Country')->sortable(),
            Select::make('Status')->options([
              'pending' => 'Pending',
              'ongoing' => 'Ongoing',
              'completed' => 'Completed',
              'failed' => 'Failed',
          ]),
            Text::make('Color')->sortable()->hideFromIndex(),
            Text::make('Hardness')->sortable()->hideFromIndex(),
            Text::make('Size')->sortable()->hideFromIndex(),
            Text::make('Dryness Process')->sortable()->hideFromIndex(),
            Text::make('Foreign Matter')->sortable()->hideFromIndex(),
            Text::make('Moisture')->sortable()->hideFromIndex(),
            Text::make('Weevil Process')->sortable()->hideFromIndex(),
            Text::make('Broken Material')->sortable()->hideFromIndex(),
            Text::make('Rotten')->sortable()->hideFromIndex(),
            Text::make('Damaged Kernel')->sortable()->hideFromIndex(),
            Text::make('Splits')->sortable()->hideFromIndex(),
            Text::make('Test Weight')->sortable()->hideFromIndex(),
            Text::make('Total Defects')->sortable()->hideFromIndex(),
            Text::make('Oil Content')->sortable()->hideFromIndex(),
            Text::make('Dockage')->sortable()->hideFromIndex(),
            Text::make('Infestations')->sortable()->hideFromIndex(),
            Text::make('Volatile')->sortable()->hideFromIndex(),
            Text::make('Hectoliter Weight')->sortable()->hideFromIndex(),
            Text::make('Mammalican')->sortable()->hideFromIndex(),
            Text::make('Ash Content')->sortable()->hideFromIndex(),
            Text::make('Curcumin Content')->sortable()->hideFromIndex(),
            Text::make('Acid Insoluble Ash')->sortable()->hideFromIndex(),
            Text::make('Insect Defiled')->sortable()->hideFromIndex(),
            Text::make('Mold By Weight')->sortable()->hideFromIndex(),
            Text::make('Extraneous By Weight')->sortable()->hideFromIndex(),
            Text::make('Whole Dead Insects')->sortable()->hideFromIndex(),
            Image::make('File Path')->path('offer_images')->hideFromIndex(),
            Text::make('Quantity')->sortable(),
            Number::make('Offer Price')->sortable(),
            Date::make('Delivery Date')->format('DD MMM YYYY')->sortable()->rules('required'),
            Place::make('Source Address')->sortable()->rules('required')->hideFromIndex(),
            Number::make('Agree Info')->sortable()->hideFromIndex(),
            Number::make('Agree Intention')->sortable()->hideFromIndex(),
            Number::make('Agree Feedback')->sortable()->hideFromIndex(),
            Number::make('Agree Penalty')->sortable()->hideFromIndex(),
            Textarea::make('Additional Information'),
            MorphMany::make('Messages'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
