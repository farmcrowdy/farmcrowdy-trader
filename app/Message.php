<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'messageable_id',
        'messageable_type',
        'sender',
        'receiver',
        'status',
        'body',
        'file_path'
    ];

    /**
     * Get the owning messageable model.
     */
    public function messageable()
    {
        return $this->morphTo();
    }

    public function send()
    {
        return $this->belongsTo(User::class, 'sender', 'id');
    }

    public function receive()
    {
        return $this->belongsTo(User::class, 'receiver', 'id');
    }

}
