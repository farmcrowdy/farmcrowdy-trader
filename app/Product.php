<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'product_type_id',
        'image_path',
    ];

    public function offer()
    {
      return $this->hasMany(Offer::class);
    }

    public function order()
    {
      return $this->hasMany(Order::class);
    }

    public function product_type()
    {
      return $this->belongsTo(ProductType::class);
    }
}
