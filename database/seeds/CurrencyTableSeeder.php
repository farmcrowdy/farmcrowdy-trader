<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Tables
        DB::table('currencies')->truncate();

        // Test data
        $currencies = [
            [
                'name' => 'US Dollars',
                'symbol' => 'USD',
            ],
            // [
            //     'name' => 'Naira',
            //     'symbol' => 'N',
            // ]
        ];
        // Load data into table...
        foreach ($currencies as $currency) {

            DB::table('currencies')->insert([
                'name' => $currency['name'],
                'symbol' => $currency['symbol'],
            ]);
        }
    }
}
