<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProduceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Tables
        DB::table('products')->truncate();

        // Test data
        $products = [
            [
                'name' => 'Maize',
                'product_type_id' => 1,
                'image_path' => 'images/maize.png'
            ],
            [
                'name' => 'Ginger',
                'product_type_id' => 1,
                'image_path' => 'images/ginger.png'
            ],
            [
                'name' => 'Rice',
                'product_type_id' => 1,
                'image_path' => 'images/rice.png'
            ],
            [
                'name' => 'Millet',
                'product_type_id' => 1,
                'image_path' => 'images/millet.png'
            ],
            [
                'name' => 'Beef',
                'product_type_id' => 2,
                'image_path' => 'images/beef.png'
            ],
            [
                'name' => 'Sorghum',
                'product_type_id' => 1,
                'image_path' => 'images/sorghum.png'
            ],
        ];
        // Load data into table...
        foreach ($products as $product) {

            DB::table('products')->insert([
                'name' => $product['name'],
                'product_type_id' => $product['product_type_id'],
                'image_path' => $product['image_path'],
            ]);
        }
    }
}
