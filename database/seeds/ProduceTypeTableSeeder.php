<?php

use Illuminate\Database\Seeder;

class ProduceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Tables
        DB::table('product_types')->truncate();

        // Test data
        $product_types = [
            [
                'name' => 'Crop',
                'image_path' => 'images/crop.png'
            ],
            [
                'name' => 'Livestock',
                'image_path' => 'images/livestock.png'
            ],
        ];
                // Load data into table...
        foreach ($product_types as $product_type) {

            DB::table('product_types')->insert([
                'name' => $product_type['name'],
                'image_path' => $product_type['image_path'],
            ]);
        }
    }
}
