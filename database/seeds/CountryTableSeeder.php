<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Tables
        DB::table('countries')->truncate();

        // Test data
        $countries = [
            [
                'sortname' => 'NG',
                'name' => 'Nigeria',
                'phonecode' => '234',
                'flag_path' => 'flags/nigeria.png',
            ],
            [
                'sortname' => 'JA',
                'name' => 'Jamaica',
                'phonecode' => '878',
                'flag_path' => 'flags/jamaica.png',
            ]
        ];
        // Load data into table...
        foreach ($countries as $country) {

            DB::table('countries')->insert([
                'sortname' => $country['sortname'],
                'name' => $country['name'],
                'phonecode' => $country['phonecode'],
                'flag_path' => $country['flag_path'],
            ]);
        }
    }
}
