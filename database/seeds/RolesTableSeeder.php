<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Tables
        DB::table('roles')->truncate();

        // Test data
        $roles = [
            [
                'name' => 'User',
                'description' => 'Regular Users',
            ],
            [
                'name' => 'Admin',
                'description' => 'Admin',
            ]
        ];
        // Load data into table...
        foreach ($roles as $role) {

            DB::table('roles')->insert([
                'name' => $role['name'],
                'description' => $role['name'],
            ]);
        }
    }
}
