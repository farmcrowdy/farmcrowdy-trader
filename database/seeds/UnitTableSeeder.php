<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate Tables
        DB::table('units')->truncate();

        // Test data
        $units = [
            [
                'name' => 'Kilograms',
                'symbol' => 'Kg',
            ],
            [
                'name' => 'Metric Tonnes',
                'symbol' => 'MT',
            ]
        ];
        // Load data into table...
        foreach ($units as $unit) {

            DB::table('units')->insert([
                'name' => $unit['name'],
                'symbol' => $unit['symbol'],
            ]);
        }
    }
}
