<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('product_id');
            $table->enum('status', ['pending','ongoing', 'completed', 'failed'])->default('pending');
            $table->string('color')->nullable();
            $table->string('hardness')->nullable();
            $table->string('size')->nullable();
            $table->string('dryness_process')->nullable();
            $table->string('foreign_matter')->nullable();
            $table->string('moisture')->nullable();
            $table->string('weevil_process')->nullable();
            $table->string('broken_material')->nullable();
            $table->string('rotten')->nullable();
            $table->string('damaged_kernel')->nullable();
            $table->string('splits')->nullable();
            $table->string('test_weight')->nullable();
            $table->string('total_defects')->nullable();
            $table->string('oil_content')->nullable();
            $table->string('dockage')->nullable();
            $table->string('infestations')->nullable();
            $table->string('volatile')->nullable();
            $table->string('hectoliter_weight')->nullable();
            $table->string('mammalican')->nullable();
            $table->string('ash_content')->nullable();
            $table->string('curcumin_content')->nullable();
            $table->string('acid_insoluble_ash')->nullable();
            $table->string('insect_defiled')->nullable();
            $table->string('mold_by_weight')->nullable();
            $table->string('extraneous_by_weight')->nullable();
            $table->string('whole_dead_insects')->nullable();
            $table->string('file_path')->nullable();
            $table->string('quantity');
            $table->foreignId('unit_id');
            $table->integer('offer_price');
            $table->dateTime('delivery_date');
            $table->foreignId('currency_id')->default('1');
            $table->foreignId('source_country_id');
            $table->mediumText('source_address');
            $table->boolean('agree_info');
            $table->boolean('agree_intention');
            $table->boolean('agree_feedback');
            $table->boolean('agree_penalty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
