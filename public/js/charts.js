// chart on insight page 
var insightChart = document.getElementById('myChart');
let lab = ["Jan",	"Feb",	"Mar",	"Apr",	"May",	"Jun",	"Jul","Aug",	"Sep","Oct"];
let labels =(axis)=>{
    return axis;
}
if (insightChart){
    let ctx = insightChart.getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: lab,
            datasets: [{
                label: 'Maize', // Name the series
                data: [500,	50,	2424,	14040,	14141,	4111,	4544,	47,	5555, 6811], // Specify the data values array
                fill: false,
                borderColor: '#86BA16', // Add custom color border (Line)
                backgroundColor: '#86BA16', // Add custom color background (Points and Fill)
                borderWidth: 1 // Specify bar border width
            }]},
        options: {
          responsive: true, // Instruct chart js to respond nicely.
          maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
        //   plugins: {
        //     zoom: {
        //         // Container for pan options
        //         pan: {
        //             // Boolean to enable panning
        //             enabled: true,
    
        //             // Panning directions. Remove the appropriate direction to disable 
        //             // Eg. 'y' would only allow panning in the y direction
        //             mode: 'xy'
        //         },
    
        //         // Container for zoom options
        //         zoom: {
        //             // Boolean to enable zooming
        //             enabled: true,
    
        //             // Zooming directions. Remove the appropriate direction to disable 
        //             // Eg. 'y' would only allow zooming in the y direction
        //             mode: 'xy',
        //         }
        //     }
        // }
        }
    });
    // get all select chart options 
    let chartOption = document.querySelectorAll(".chartOption");
    for (const selectInput of chartOption){
        selectInput.addEventListener("change", updateChart);
    }

    // Function updates a particular chart from the select options
    function updateChart() {
    myChart = new Chart(ctx, {
        type: "line",
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July", "aug", "sept", "oct"],
            datasets: [{
                label: 'Maize', // Name the series
                data: [20,	500,	2424,	14040,	14141,	41011,	4544,	47,	5555, 6811], // Specify the data values array
                fill: false,
                borderColor: '#86BA16', // Add custom color border (Line)
                backgroundColor: '#86BA16', // Add custom color background (Points and Fill)
                borderWidth: 1 // Specify bar border width
            }]},
    });
    myChart.update();

    };
}
// transaction charts
var transaxtionChart = document.getElementById('transaction_chart');
if (transaxtionChart){
    // chart 1
    let chart_1 = document.getElementById('myChart_1').getContext('2d');
    // chart 2
    let chart_2 = document.getElementById('myChart_2').getContext('2d');
// function that executes the charts 
    let myChart = (ctx)=>{
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: lab,
                datasets: [{
                    label: 'Maize', // Name the series
                    data: [500,	50,	2424,	14040,	14141,	4111,	4544,	47,	5555, 6811], // Specify the data values array
                    fill: false,
                    borderColor: '#86BA16', // Add custom color border (Line)
                    backgroundColor: '#86BA16', // Add custom color background (Points and Fill)
                    borderWidth: 1 // Specify bar border width
                }]},
            options: {
              responsive: true, // Instruct chart js to respond nicely.
              maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
            }
        });
    } 
    myChart(chart_1);
    myChart(chart_2);
    // get all select chart options for chart 1
    let chartOption1 = document.querySelectorAll(".chartOption1");
    for (const selectInput1 of chartOption1){
        selectInput1.addEventListener("change", updateChart);
    }

      // get all select chart options for chart 2
      let chartOption2 = document.querySelectorAll(".chartOption2");
      for (const selectInput2 of chartOption2){
          selectInput2.addEventListener("change", updateChart);
      }

    // Function updates a particular chart from the select options
    function updateChart() {
        
        myChartUpdate = (update)=>{
            let myChart = new Chart(update, {
                type: "line",
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "aug", "sept", "oct"],
                    datasets: [{
                        label: 'Maize', // Name the series
                        data: [20,	500,	2424,	14040,	14141,	41011,	4544,	47,	5555, 6811], // Specify the data values array
                        fill: false,
                        borderColor: '#86BA16', // Add custom color border (Line)
                        backgroundColor: '#86BA16', // Add custom color background (Points and Fill)
                        borderWidth: 1 // Specify bar border width
                    }]},
            });
            myChart.update();
        }
    myChartUpdate(chart_1);
    myChartUpdate(chart_2);

    };
}

