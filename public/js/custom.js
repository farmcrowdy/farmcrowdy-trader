// buyer checkbox toggle
let boxes = document.querySelectorAll('input.toggler[type="radio"]');
let span = document.getElementById('userStatus');
boxes = Array.prototype.slice.call(boxes);

boxes.forEach((box)=> {
  box.addEventListener('change', (e)=> {
    let status = e.currentTarget.value;
    status === "on" ? span.innerHTML = "Supplier" : span.innerHTML = "Buyer"
  });
});

// data table
let paymentTable = document.getElementById('paymentTable');
if(paymentTable){
  $('#PaymentDataTable').DataTable();
  // make the table in the tab responsive
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable()
       .columns.adjust()
       .responsive.recalc();
});
}
// insight data table
let insightTable = document.getElementById('insightTable');
if(insightTable){
  $('#insightDataTable').DataTable();
}
// inbox table
let inboxTable = document.getElementById('inboxDataTable');
if(inboxTable){
//console.log(inboxTable)

  $('#inboxDataTable').DataTable();
}
/* Check the location of each element */
$('.content').each( function(i){
  var bottom_of_object= $(this).offset().top + $(this).outerHeight();
  var bottom_of_window = $(window).height();
  // console.log($(this).offset().top)
  if( bottom_of_object > bottom_of_window){
    $(this).addClass('hidden');
  }
});


$(window).scroll( function(){
    /* Check the location of each element hidden */
    $('.hidden').each( function(i){

        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        /* If the object is completely visible in the window, fadeIn it */
        if( bottom_of_window > bottom_of_object ){
          $(this).animate({'opacity':'1'},700);
        }
    });
});

$(document).ready(function(){
  let stepOne, inputOne, inputTwo, inputThree, inputFour, inputFive, stepTwo, input6, input7, input8, input9, input10, input11, input12, input13, check1, check2, check3, check4;
  $("#productID").change(()=>{
    inputOne = $("#productID").val()!="";
  })
  $("#color").change(()=>{
    inputTwo = $("#color").val()!="";
  })
  $("#size").change(()=>{
    inputThree = $("#size").val()!="";
  })
  $("#hardness").change(()=>{
    inputFour = $("#hardness").val()!="";
  })
  $("#Dryness").change(()=>{
    inputFive = $("#Dryness").val()!="";
  })//step two
  $("#Quantity").change(()=>{
    input6 = $("#Quantity").val()!="";
  })
  $("#preferredUnit").change(()=>{
    input7 = $("#preferredUnit").val()!="";
  })
  $("#preferredCurrency").change(()=>{
    input8 = $("#preferredCurrency").val()!="";
  })
  $("#demandPrice").change(()=>{
    input9 = $("#demandPrice").val()!="";
  })
  $("#deliveryDate").change(()=>{
    input10 = $("#deliveryDate").val()!="";
  })
  $("#preferredLocation").change(()=>{
    input11 = $("#preferredLocation").val()!="";
  })
  $("#preferredCountry").change(()=>{
    input12 = $("#preferredCountry").val()!="";
  })
  $("#uploadFiles").change(()=>{
    input13 = $("#uploadFiles").val()!="";
    $("#imgUploadLabel span").html($("#uploadFiles").val());
  })
  $("#check1").change(()=>{
    check1 = $("#check1").is(":checked");
  })
  $("#check2").change(()=>{
    check2 = $("#check2").is(":checked");
  })
  $("#check3").change(()=>{
    check3 = $("#check3").is(":checked");
  })
  $("#check4").change(()=>{
    check4 = $("#check4").is(":checked");
  })

  $(".form-wrapper .button").click(function(){

    stepOne = inputOne && inputTwo && inputThree && inputFour && inputFive;
    stepTwo = input6 && input7 && input8 && input9 && input10 && input11 && input12 &&  check1 &&  check2 &&  check3 &&  check4;

    var button = $(this);
    var currentSection = button.parents(".section");
    var currentSectionIndex = currentSection.index();
    // check if rquired inputs are filled
    // if(currentSectionIndex === 0 && stepOne){
    if(currentSectionIndex === 1 && stepOne){
      var headerSection = $('.steps li').eq(currentSectionIndex - 1);
      currentSection.removeClass("is-active").next().addClass("is-active");
      headerSection.addClass("success_");
      headerSection.removeClass("is-active").next().addClass("is-active");
    }
      // check if the important fields in the second step has been field
      if(currentSectionIndex === 2 && stepTwo){
      var headerSection = $('.steps li').eq(currentSectionIndex - 1);
        currentSection.removeClass("is-active").next().addClass("is-active");
        headerSection.addClass("success_");
        headerSection.removeClass("is-active").next().addClass("is-active");
    }
    // check if review page has empty field and hide the element
    // if(currentSectionIndex === 1){
      for( const span of $(".output_container li span")){
          (span.innerHTML) == "Nill" ? $(span).parent().css("display","none"): $(span).parent().css("display","block")
      }
    // }

    $(".form-wrapper").submit(function(e) {
      // e.preventDefault();
    });
    // check fields for 2
    // if(currentSectionIndex === 3){
    //   $(document).find(".form-wrapper .section").first().addClass("is-active");
    //   $(document).find(".steps li").first().addClass("is-active");
    // }
  });

  $(".form-wrapper .prev").click(function(){
    var button = $(this);
    var currentSection = button.parents(".section");
    var currentSectionFloor = currentSection.index();
    var currentSectionIndex = (currentSectionFloor - 1);
    var headerSection = $('.steps li').eq(currentSectionIndex);
    currentSection.removeClass("is-active").prev().addClass("is-active");
    headerSection.removeClass("is-active success_").prev().addClass("is-active");

    $(".form-wrapper").submit(function(e) {
      e.preventDefault();
    });

    // if(currentSectionIndex === 3){
    //   $(document).find(".form-wrapper .section").first().addClass("is-active");
    //   $(document).find(".steps li").first().addClass("is-active");
    // }
  });
});
// file drag and upload
// const inputElement = document.querySelector('input.file_upload[type="file"]');
// inputElement ? FilePond.create( inputElement, {maxFiles : 3} ) : " ";

// output form in the page for form preview
let inputFields = document.querySelectorAll(".textButton");
for( const innerText of inputFields){
  innerText.addEventListener("input", (e) => {
    // get the value of the field
    let inputValue;
    if ($(e.target).is('select')){
      inputValue = $(`#${e.target.id} option:selected`).text();
    }else{
      inputValue = e.target.value;
    }
    // create a new id from the field's id
    let inputId = `${e.target.id}Span`;
    // get the elemnt with the created id
    let outputSpan= document.getElementById(inputId);
    // output the result in the page
   return  outputSpan.innerHTML = inputValue;
  });
}
// $(".output_container li span").innerHTML == Nill ? $(".output_container li span").css("display","none"):$(".output_container li span").css("display","inline-block")
let fileUpload = document.querySelector(".uploadFiles");
if(fileUpload){
  fileUpload.addEventListener("change", (e) => {
    let span = document.getElementById('fileUploadSpan');
    // span.innerHTML = ""
      span.innerHTML = e.target.value;
  })
}
// For profile page
let profileEdit = document.getElementById("profile_container");
if(profileEdit){
  for( const innerText of inputFields){
      // get the value of the field
      let inputValue = innerText.value;
      // create a new id from the field's id
      let inputId = `${innerText.id}Span`;
      // get the elemnt with the created id
      let outputSpan= document.getElementById(inputId);
      // output the result in the page
       outputSpan.innerHTML = inputValue;
  }

  $('#upload-file-selector').change(function () {
    var files = $(this).get(0).files;
    if (files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var previewContainer = $('#image-preview');
            previewContainer.find('img').attr('src', e.target.result).removeClass('hidden');
            previewContainer.find('.user_placeholder').addClass('hidden');
        };

        reader.readAsDataURL(files[0]);
    }
})
}
// word count for posts on enquiry page
$("div.post_body p.post_detail").text(function(index, currentText) {
  return currentText.substr(0, 175) + "...";
})
// word count for inbox msg on inbox page
$("div.users_msg p.messagers").text(function(index, currentText) {
  return currentText.substr(0, 170) + "...";
})

// payment modal
let paymentModal = document.getElementById("myModal");
let closeSpan = document.querySelector(".close_modal");
// Get all links that opens the modal
let fireModalBtn = document.querySelectorAll(".make_payment");
if(paymentModal){
  for (const element of fireModalBtn) {
        // When a user clicks submit, open the modal
        element.onclick = ()=> {
            paymentModal.style.display = "block";
          }
  }
  // When the user clicks on <span> (x), close the modal
  closeSpan.onclick = ()=> {
    paymentModal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = (event)=> {
    if (event.target == paymentModal) {
      paymentModal.style.display = "none";
    }
  }
}
