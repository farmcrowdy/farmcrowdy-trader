@if ($products->lastPage() > 1)
<footer>
    <div class="msg_numbers">
        <p>Displaying {{$products->currentPage()}} out of {{$products->lastPage()}}</p>
    </div>
    <div class="msg_pagination">
        <ul>
            @if($products->currentPage() == 1)
            <li class="{{ ($products->currentPage() == 1) ? ' active' : '' }}">
                <a href="{{ $products->url(1) }}">
            @else
            <li>
                <a href="{{ $products->url($products->currentPage() - 1) }}">
            @endif
                <img src="{{asset('images/icons/left1.svg')}}" alt="Left arrow"></a>
            </li>
            @for ($i = 1; $i <= $products->lastPage(); $i++)
                <li class="{{ ($products->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $products->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            @if($products->currentPage() == $products->lastPage())
            <li>
                <a href="{{ $products->url($products->currentPage()) }}">
            @else
            <li class="{{ ($products->currentPage() == $products->lastPage()) ? ' active' : '' }}">
                <a href="{{ $products->url($products->currentPage()+1) }}">
            @endif
                <img src="{{asset('images/icons/right2.svg')}}" alt="Right arrow"></a>
            </li>
        </ul>
    </div>
</footer>
@endif