@extends('layouts.fctrader')
@section('nav')
@include('fctrader.nav')
@endsection('nav')
@section('content')
    <main>
        @include('fctrader.page_header')
        <div class="main_body">
            <!-- offer creation section  -->
           <section class="create_offer_">
               <div class="offer_details_">
                   <div class="head">Get an Offer Today</div>
                   <div class="title">Get Competitive Offers from
                    Verified Suppliers and Buyers today.</div>
                    <div class="order_flex">
                    <a href="{{ route('create_offer') }}">Sell harvest</a>
                    <a href="{{ route('create_order') }}">Buy harvest</a>
                   </div>
                   {{-- <div>
                    <a href="{{ route('create_order') }}">Create an Order or Offer</a>
                   </div> --}}

               </div>
               <div class="offer_image_">
                   <img src="{{asset('images/rice.png')}}" alt="Offer">
               </div>
           </section>
            <!-- end of offer creation section  -->
            <!-- insight section  -->
           <section class="insight_section">
            <div class="insight_chart">
                <img src="{{asset('images/insight_chart.svg')}}" alt="Insight chart">
            </div>
            <div class="insight_link">
                <div class="head">
                    <h4>Do More with Insights.</h4>
                </div>
                <div class="link_">
                    <a href="{{url('insights')}}">See Insight</a>
                </div>
            </div>
           </section>
            <!--end of insight section  -->
           <!-- offer listing  -->
            <section class="offer_listing">
                <div class="listing_container">
                    <div class="listing_header">
                        <div class="title">
                            <h6>Browse Offers</h6>
                        </div>
                        <div class="search">
                            <form method="GET" action="{{ route('search_offer') }}">
                                <label for="search_">
                                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13.8618 13.0515L10.1838 9.37347C11.1126 8.23164 11.568 6.77695 11.4559 5.30931C11.3438 3.84167 10.6729 2.47298 9.5814 1.48544C8.48994 0.497897 7.06117 -0.0332146 5.58967 0.00161043C4.11818 0.0364355 2.71613 0.634542 1.67261 1.67261C0.634542 2.71613 0.0364355 4.11818 0.00161043 5.58967C-0.0332146 7.06117 0.497897 8.48994 1.48544 9.5814C2.47298 10.6729 3.84167 11.3438 5.30931 11.4559C6.77695 11.568 8.23164 11.1126 9.37347 10.1838L13.0515 13.8618C13.1614 13.956 13.3029 14.0052 13.4475 13.9996C13.5921 13.994 13.7293 13.934 13.8317 13.8317C13.934 13.7293 13.994 13.5921 13.9996 13.4475C14.0052 13.3029 13.956 13.1614 13.8618 13.0515ZM2.48293 8.98843C1.83965 8.34557 1.40148 7.52638 1.22384 6.63446C1.0462 5.74255 1.13707 4.81798 1.48495 3.97772C1.83284 3.13746 2.42212 2.41924 3.17824 1.91393C3.93437 1.40861 4.82337 1.13889 5.73281 1.13889C6.64224 1.13889 7.53124 1.40861 8.28737 1.91393C9.04349 2.41924 9.63277 3.13746 9.98066 3.97772C10.3285 4.81798 10.4194 5.74255 10.2418 6.63446C10.0641 7.52638 9.62596 8.34557 8.98268 8.98843C8.55774 9.41848 8.05168 9.75991 7.49381 9.99295C6.93595 10.226 6.33738 10.346 5.73281 10.346C5.12823 10.346 4.52966 10.226 3.9718 9.99295C3.41393 9.75991 2.90787 9.41848 2.48293 8.98843Z" fill="#263308"/>
                                    </svg>
                                    <input type="search" name="search_offer" placeholder="Search for any offer" id="search_">
                                </label>
                            </form>
                        </div>
                        <div class="filter">
                            <select name="recent" id="">
                                <option value="">All</option>
                                <option value="">1 day ago</option>
                            </select>
                        </div>
                    </div>
                    @if(!empty($products))
                    <div class="offer_lists">
                        @foreach ( $products as $product )
                        @if($product instanceof App\Offer)
                        <a href="{{ url('offer_details'.'/'. $product->id)}}" class="created_offer">
                            <div class="img_con">
                                    @if (!empty($product->file_path))
                                    <img src="{{asset(str_replace('public','storage',$product->file_path))}}" alt="Offer image">
                                    @else
                                    <img src="{{asset('images/no-image.png')}}" alt="Offer image">
                                    @endif
                                <span class="supplier_offer">Offer</span>
                        @else
                        <a href="{{ route('order_details').'/'. $product->id}}" class="created_offer">
                            <div class="img_con">
                                    @if (!empty($product->file_path))
                                    <img src="{{asset(str_replace('public','storage',$product->file_path))}}" alt="Order image">
                                    @else
                                    <img src="{{asset('images/no-image.png')}}" alt="Order image">
                                    @endif
                                <span class="buy_offer">Order</span>
                                @endif
                            </div>
                            <div class="detail">
                                <p class="name">
                                    <span>{{$product->product->name}}</span> &nbsp;
                                    @if(Auth::user()->id == $product->user->id)
                                        {{$product->user->first_name .' '.$product->user->last_name }}
                                    @endif
                                    </p>
                                <ul>
                                    @if(empty($product->demand_price))
                                    <li>{{ $product->currency->symbol.number_format($product->offer_price,2)}}/{{$product->unit->name}}</li>
                                    @else
                                    <li>{{ $product->currency->symbol.number_format($product->demand_price,2)}}/{{$product->unit->name}}</li>
                                    @endif
                                    <li>{{number_format($product->quantity)}} {{$product->unit->name}}</li>
                                    <li>Status: {{$product->status}}</li>
                                </ul>
                            </div>
                        </a>
                        @endforeach
                    </div>
                    @endif
                    @include('fctrader.pagination')
                </div>
            </section>
           <!-- end of offer listing  -->

        </div>
    </main>
@endsection('content')

@section('scripts')
<script src="{{ asset('js/fileUploader.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
@endsection('scripts')
