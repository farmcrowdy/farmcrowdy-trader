<div class="page_header">
    <div class="notify drop_notification" >
        <!-- remove "span_pulse_button" class when notification is empty  -->
        <a href="javascript:void(0)" class="notification_container" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class=" span_">.</span>
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                <path d="M12.9813 11.5485V6.72412C12.9813 4.48267 11.4916 2.58289 9.45044 1.96216C9.45093 1.95312 9.45178 1.94421 9.45178 1.93506V1.53076C9.45178 0.686768 8.80273 0 8.005 0C7.20728 0 6.55823 0.686768 6.55823 1.53076V1.93506C6.55823 1.94421 6.55908 1.95312 6.55957 1.96216C4.51843 2.58289 3.02869 4.48267 3.02869 6.72412V11.5485C2.34131 11.6519 1.8125 12.2465 1.8125 12.9623V13.7097C1.8125 14.0303 2.07336 14.291 2.39392 14.291H5.87866C6.09375 15.2672 6.96521 16 8.005 16C9.0448 16 9.91626 15.2672 10.1313 14.291H13.6161C13.9368 14.291 14.1975 14.0303 14.1975 13.7097V12.9623C14.1975 12.2465 13.6688 11.6519 12.9813 11.5485ZM7.49573 1.53076C7.49573 1.20374 7.72424 0.9375 8.005 0.9375C8.28577 0.9375 8.51428 1.20374 8.51428 1.53076V1.77368C8.3468 1.75659 8.17688 1.7478 8.005 1.7478C7.83313 1.7478 7.66321 1.75659 7.49573 1.77368V1.53076ZM3.96619 6.72412C3.96619 4.49707 5.77795 2.6853 8.005 2.6853C10.2321 2.6853 12.0438 4.49707 12.0438 6.72412V11.5325H3.96619V6.72412ZM8.005 15.0625C7.48694 15.0625 7.04211 14.7432 6.85681 14.291H9.1532C8.9679 14.7432 8.52307 15.0625 8.005 15.0625ZM13.26 13.3535H2.75V12.9623C2.75 12.6908 2.97083 12.4698 3.24231 12.4698H12.7677C13.0392 12.4698 13.26 12.6908 13.26 12.9623V13.3535Z" fill="#213131" fill-opacity="0.7"/>
                </g>
                <defs>
                <clipPath id="clip0">
                <rect width="16" height="16" fill="white"/>
                </clipPath>
                </defs>
            </svg>
        </a>
        <!-- notification dropdown  -->
        {{-- <div class="prompt dropdown-menu">
            <div class="scroll_"> --}}
            {{-- <ul>
                <li><a href="javascript:void(0)">Cart Item One</a></li>
                <li><a href="javascript:void(0)">Cart Item One</a></li>
                <li><a href="javascript:void(0)">Cart Item One</a></li>
                <li><a href="javascript:void(0)">
                    <div>Cart Item One Cart Item One Cart Item One Cart Item One Cart Item One Cart Item One</div>
                </a></li>
                <li><a href="javascript:void(0)">Cart Item One</a></li>
                <li><a href="javascript:void(0)">Cart Item One</a></li>
                <li><a href="javascript:void(0)">Cart Item One</a></li>
            </ul> --}}
            {{-- </div>
        </div> --}}
        <!-- end of notification dropdown  -->

    </div>

    <div class="msg_ drop_notification">
        {{-- <a href="javascript:void(0)" class="notification_container" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false"> --}}
          <a href="mailto:fctraderadmins@farmcrowdy.com" class="notification_container" >
            <!-- remove "span_pulse_button" class when notification is empty  -->
            {{-- <span class=" span_">.</span> --}}
            <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M19.0312 0H1.96875C0.883118 0 0 0.897135 0 2V14C0 15.1029 0.883118 16 1.96875 16H19.0312C20.1169 16 21 15.1029 21 14V2C21 0.897135 20.1169 0 19.0312 0ZM18.6729 1.33333L10.9377 8.37305C10.6884 8.60026 10.3117 8.60026 10.0621 8.37305L2.3273 1.33333H18.6729ZM19.6875 14C19.6875 14.3675 19.3932 14.6667 19.0312 14.6667H1.96875C1.60682 14.6667 1.3125 14.3675 1.3125 14V2.19963L9.18622 9.36589C9.56081 9.70703 10.0304 9.87728 10.5 9.87728C10.9696 9.87728 11.4392 9.70671 11.8136 9.36589L19.6875 2.19977V14Z" fill="#213131" fill-opacity="0.7"/>
            </svg>
        </a>
        <!-- message notification dropdown  -->
        {{-- <div class="prompt dropdown-menu">
            <div class="scroll_"> --}}
                {{-- <ul>

                    <li><a href="#">
                        <div><span>#Maize</span> @Sinzu_money</div>
                        <div class="msg">Do you have samples samples samples</div>
                        <span class="time">5 hours ago</span>
                    </a></li>
                    <li><a href="#">Cart Item Three</a></li>
                    <li><a href="#">Cart Item Three</a></li>
                </ul>
                <div class="see_all">
                    <a href="#" class="button">View all</a>
                </div> --}}
            {{-- </div>
        </div> --}}
        <!-- end of message notification dropdown  -->

    </div>
    <a class="logout" href="{{ route('logout') }}">Logout</a>
</div>
