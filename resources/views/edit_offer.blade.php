@extends('layouts.fctrader')

@section('nav')
@include('fctrader.nav')
@endsection('nav')
@section('content')
    <main>
    @include('fctrader.page_header')
        <div class="main_body">
            <div class="header__">
                <h4 class="title">Quality Specification</h4>
                @if($errors->any())
                    <div class="ml-4 alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Please check the fields in red for correction.</strong>
                    </div>
                @endif
            </div>

            <div class="create_offer_form">
                <div class="form_step_container">
                    <div class="wrapper">
                        <ul class="steps">
                            <li class="is-active">
                                <div class="position">
                                    <span>1</span>
                                </div>
                                <p>Quality &amp; Quantity
                                    Specification</p>
                            </li>
                            <li>
                                <div class="position">
                                    <span>2</span>
                                </div>
                                <p>Delivery Location &amp; Terms
                                    of Payment</p>
                            </li>
                            <li>
                                <div class="position">
                                    <span>3</span>
                                </div>
                                <p>Review Specifications filled
                                    in Part One &amp; Two</p>
                            </li>
                        </ul>
                        <form class="form-wrapper" action="{{ url('update_offer') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{Auth::user()->id}}" />
                            <input type="hidden" name="offer_id" value="{{$offer->id}}" />
                            <fieldset class="section is-active">
                                <h4>Crop Category</h4>
                                <div class="underline"></div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="product_id">Produce</label>
                                        <select name="product_id" id="productID" class="textButton">
                                            @foreach ($products as $product)
                                            <option value="{{$product->id}}" @if( old('product_id', $offer->product->id)  == $product->id) selected="selected" @endif>{{$product->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input_container">
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="color">Color</label>
                                        <input name="color" type="text" id="color" class="textButton" placeholder="White" value="{{old('color', $offer->color)}}">
                                        @error('color')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="size">Size</label>
                                        <input name="size" type="text" id="size" class="textButton" placeholder="Whole Grain" value="{{old('size', $offer->size)}}">
                                        @error('size')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="hardness">Hardness</label>
                                        <input name="hardness" type="text" id="hardness" class="textButton" placeholder="Soft" value="{{old('hardness', $offer->hardness)}}">
                                        @error('hardness')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Dryness">Dryness Process</label>
                                        <input name="dryness_process" type="text" id="Dryness" class="textButton" placeholder="Air dried" value="{{old('dryness_process', $offer->dryness_process)}}">
                                        @error('dryness_process')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <h4 class="form_header">Quality Specifications</h4>
                                <div class="underline"></div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Matter">Foreign Matter</label>
                                        <input name="foreign_matter" type="text" id="Matter" class="textButton" placeholder="0%" value="{{old('foreign_matter', $offer->foreign_matter)}}">
                                        @error('foreign_matter')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Moisture">Moisture</label>
                                        <input name="moisture" type="text" id="Moisture" class="textButton" placeholder="0%" value="{{old('moisture', $offer->moisture)}}">
                                        @error('moisture')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Weevil">Weevil</label>
                                        <input name="weevil_process" type="text" id="Weevil" class="textButton" placeholder="0%" value="{{old('weevil_process', $offer->weevil_process)}}">
                                        @error('weevil_process')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="brokenMaterial">Broken Material</label>
                                        <input name="broken_material" type="text" id="brokenMaterial" class="textButton" placeholder="0%" value="{{old('broken_material', $offer->broken_material)}}">
                                        @error('broken_material')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Rotten">Rotten/Shrivelled</label>
                                        <input name="rotten" type="text" id="Rotten" class="textButton" placeholder="0%" value="{{old('rotten', $offer->rotten)}}">
                                        @error('rotten')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="damagedKernel">Damaged Kernel</label>
                                        <input name="damaged_kernel" type="text" id="damagedKernel" class="textButton" placeholder="0%" value="{{old('damaged_kernel', $offer->damaged_kernel)}}">
                                        @error('damaged_kernel')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Splits">Splits</label>
                                        <input name="splits" type="text" id="Splits" class="textButton" placeholder="0%" value="{{old('splits', $offer->splits)}}">
                                        @error('splits')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Weight">Test Weight</label>
                                        <input name="test_weight" type="text" id="Weight" class="textButton" placeholder="0%" value="{{old('test_weight', $offer->test_weight)}}">
                                        @error('test_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Defects">Total Defects</label>
                                        <input name="total_defects" type="text" id="Defects" class="textButton" placeholder="0%" value="{{old('total_defects', $offer->total_defects)}}">
                                        @error('total_defects')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="oilContent">Oil Content</label>
                                        <input name="oil_content" type="text" id="oilContent" class="textButton" placeholder="0%" value="{{old('oil_content', $offer->oil_content)}}">
                                        @error('oil_content')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Dockage">Dockage</label>
                                        <input name="dockage" type="text" id="Dockage" class="textButton" placeholder="0%" value="{{old('dockage', $offer->dockage)}}">
                                        @error('dockage')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Infestations">Infestations</label>
                                        <input name="infestations" type="text" id="Infestations" class="textButton" placeholder="0%" value="{{old('infestations',  $offer->infestations)}}">
                                        @error('infestations')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Volatile">Volatile</label>
                                        <input name="volatile" type="text" id="Volatile" class="textButton" placeholder="0%" value="{{old('volatile', $offer->volatile)}}">
                                        @error('volatile')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="hectoliterWeight">Hectoliter Weight</label>
                                        <input name="hectoliter_weight" type="text" id="hectoliterWeight" class="textButton" placeholder="0%" value="{{old('hectoliter_weight', $offer->hectoliter_weight)}}">
                                        @error('hectoliter_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Mammalican">Mammalican</label>
                                        <input name="mammalican" type="text" id="Mammalican" class="textButton" placeholder="0%" value="{{old('mammalican', $offer->mammalican)}}">
                                        @error('mammalican')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="ashContent">Ash Content</label>
                                        <input name="ash_content" type="text" id="ashContent" class="textButton" placeholder="0%" value="{{old('ash_content', $offer->ash_content)}}">
                                        @error('ash_content')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="curcuminContent">Curcumin Content</label>
                                        <input name="curcumin_content" type="text" id="curcuminContent" class="textButton" placeholder="0%" value="{{old('curcumin_content', $offer->curcumin_content)}}">
                                        @error('curcumin_content')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="acidInsolubleAsh">Acid Insoluble Ash</label>
                                        <input name="acid_insoluble_ash" type="text" id="acidInsolubleAsh" class="textButton" placeholder="0%" value="{{old('acid_insoluble_ash', $offer->acid_insoluble_ash)}}" >
                                        @error('acid_insoluble_ash')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="insectDefiled">Insect Defiled/Infested % by Weight</label>
                                        <input name="insect_defiled" type="text" id="insectDefiled" class="textButton" placeholder="0%" value="{{old('insect_defiled', $offer->insect_defiled)}}">
                                        @error('insect_defiled')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="moldWeight">Mold % by Weight</label>
                                        <input name="mold_by_weight" type="text" id="moldWeight" class="textButton" placeholder="0%" value="{{old('mold_by_weight', $offer->mold_by_weight)}}">
                                        @error('mold_by_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="extraneousWeight">Extraneous % by Weight</label>
                                        <input name="extraneous_by_weight" type="text" id="extraneousWeight" class="textButton" placeholder="0%" value="{{old('extraneous_by_weight', $offer->extraneous_by_weight)}}">
                                        @error('extraneous_by_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="wholeDeadInsects">Whole Dead Insects</label>
                                        <input name="whole_dead_insects" type="text" id="wholeDeadInsects" class="textButton" placeholder="0%" value="{{old('whole_dead_insects', $offer->whole_dead_insects)}}">
                                        @error('whole_dead_insects')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <h4 class="form_header">Additional Information</h4>
                                <p>Offer Image</p>
                                <div class="underline"></div>
                                @if (!empty($offer->file_path))
                                <img class="m-4" src="{{asset(str_replace('public','storage',$offer->file_path))}}" alt="Order image">
                                @else
                                <img class="m-4" src="{{asset('images/no-image.png')}}" alt="Order image">
                                @endif
                                <div class="upload_file">
                                    <input name="file_path" type="file" class="textButton" id="fileUpload"/>
                                    @error('file_path')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="btn_groups">
                                    <div class="button next_btn" data-id="2">Next Step</div>
                                </div>
                            </fieldset>
                            <fieldset class="section">
                                <h4 class="">Quality Specifications</h4>
                                <div class="underline"></div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Quantity">Total Quantity</label>
                                        <input name="quantity" type="text" id="Quantity" placeholder="5000" class="textButton" value="{{old('quantity', $offer->quantity)}}">
                                        @error('quantity')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="preferredUnit">Preferred Volume Unit</label>
                                        <select name="unit_id" id="preferredUnit" class="textButton">
                                            @foreach ($units as $unit)
                                            <option value="{{$unit->id}}" @if( old('unit_id', $offer->unit_id)  == $unit->id) selected="selected" @endif>{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="preferredCurrency">Preferred Currency</label>
                                        <select name="currency_id" id="preferredCurrency" class="textButton">
                                            @foreach ($currencies as $currency)
                                            <option value="{{$currency->id}}" @if( old('currency_id', $offer->currency_id)  == $currency->id) selected="selected" @endif>{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input_container">
                                        <label for="demandPrice">Price per unit</label>
                                        <input name="offer_price" type="number" id="demandPrice" class="textButton" placeholder="6000" value="{{old('offer_price', $offer->offer_price)}}">
                                        @error('offer_price')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                    </div>
                                    <div class="input_container">
                                        <label for="deliveryDate">Delivery Date Deadline</label>
                                        <input type="date" name="delivery_date" id="deliveryDate" class="textButton" value="{{old('delivery_date', date('Y-m-d', strtotime($offer->delivery_date)))}}">
                                        @error('delivery_date')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="preferredLocation">Preferred Delivery Location</label>
                                        <input name="source_address" type="text" id="preferredLocation" class="textButton" placeholder="#Address" value="{{old('source_address', $offer->source_address)}}">
                                        @error('source_address')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="preferredCountry">Preferred Delivery Location Country</label>
                                        <select name="source_country_id" id="preferredCountry" class="textButton">
                                            @foreach ($countries as $country)
                                            <option value="{{$country->id}}" @if( old('source_country_id', $offer->source_country_id)  == $country->id) selected="selected" @endif>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <h4 class="form_header">Agreement</h4>
                                <div class="underline"></div>
                                <div class="terms_group">
                                    <div class="checkbox_container">
                                        <input type="checkbox" name="agree_info" id="check1" class="agreement_box" @if( old('agree_info', $offer->agree_info)  == '1') checked @endif value="1">
                                        <label for="check1">
                                            <div class="check_box" 
                                            @error('agree_info')
                                            style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                            @enderror>
                                            </div> I agree that all the information i have provided
                                            are correct and true to my best knowledge.
                                        </label>
                                    </div>
                                    <div class="checkbox_container">
                                        <input type="checkbox" name="agree_intention" id="check2" class="agreement_box" @if( old('agree_intention', $offer->agree_intention)  == '1') checked @endif value="1">
                                        <label for="check2">
                                            <div class="check_box"
                                            @error('agree_intention')
                                            style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                            @enderror></div> I agree that the company that I am affiliated with
                                            has sincere intention of actually purchasing this products from suitable
                                            suppliers without any
                                            intention to purely check suppliers’ prices or benchmark competitors’ prices
                                            without the intent to purchase.
                                        </label>
                                    </div>
                                    <div class="checkbox_container">
                                        <input type="checkbox" name="agree_feedback" id="check3" class="agreement_box" @if( old('agree_feedback', $offer->agree_info)  == '1') checked @endif value="1">
                                        <label for="check3">
                                            <div class="check_box"
                                            @error('agree_feedback')
                                            style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                            @enderror></div> I agree to provide proper feedback on any offers
                                            proposed to me by any of the suppliers. I also agree to respond to any of the
                                            supplier’s in a
                                            timely manner.
                                        </label>
                                    </div>
                                    <div class="checkbox_container">
                                        <input type="checkbox" name="agree_penalty" id="check4" class="agreement_box" @if( old('agree_penalty', $offer->agree_penalty)  == '1') checked @endif value="1">
                                        <label for="check4">
                                            <div class="check_box"
                                            @error('agree_penalty')
                                            style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                            @enderror></div> I understand that, any company can be penalized or
                                            permanently blocked on Farmcrowdy Traders’ platform if any points agreed here
                                            turns out
                                            to be false.
                                        </label>
                                    </div>
                                </div>
                                <div class="btn_groups">
                                    <div class="prev next_btn">Previous Step</div>
                                    <div class="button next_btn" data-id="3">Next Step</div>
                                </div>

                            </fieldset>
                            <fieldset class="section">
                                <h4 class="">Crop Category</h4>
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>
                                        <li>Crop :<span id="productIDSpan">Nil</span></li>
                                        <li>Color: <span id="colorSpan">Nil</span></li>
                                        <li>Hardness: <span id="hardnessSpan">Nil</span></li>
                                        {{-- <li>Crop Type: <span id="cropTypeSpan">Nil</span></li> --}}
                                        <li> Size: <span id="sizeSpan">Nil</span></li>
                                        <li>Drying Process: <span id="DrynessSpan">Nil</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Quality Specifications</h4>
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>
                                        <li>Foreign Matter: <span id="MatterSpan">Nil</span></li>
                                        <li> Weevil: <span id="WeevilSpan">Nil</span></li>
                                        <li>Rotten/Shrivelled: <span id="RottenSpan">Nil</span></li>
                                        <li> Splits: <span id="SplitsSpan">Nil</span></li>
                                        <li> Total Defects: <span id="DefectsSpan">Nil</span></li>
                                        <li> Dockage: <span id="DockageSpan">Nil</span></li>
                                        <li> Volatile: <span id="VolatileSpan">Nil</span></li>
                                        <li> Mammalican: <span id="MammalicanSpan">Nil</span></li>
                                        <li> Curcumin Content: <span id="curcuminContentSpan">Nil</span></li>
                                        <li> Insect Defiled/Infested % by Weight: <span id="insectDefiledSpan">Nil</span>
                                        </li>
                                        <li> Extraneous % by Weight: <span id="extraneousWeightSpan">Nil</span></li>
                                        <li> Moisture: <span id="MoistureSpan">Nil</span></li>
                                        <li> Broken Material: <span id="brokenMaterialSpan">Nil</span>
                                        <li>
                                        <li> Damaged Kernel: <span id="damagedKernelSpan">Nil</span></li>
                                        <li> Test Weight: <span id="WeightSpan">Nil</span></li>
                                        <li> Oil Content: <span id="oilContentSpan">Nil</span></li>
                                        <li> Infestations: <span id="InfestationsSpan">Nil</span></li>
                                        <li> Hectoliter Weight: <span id="hectoliterWeightSpan">Nil</span></li>
                                        <li> Ash Content: <span id="ashContentSpan">Nil</span></li>
                                        <li> Acid Insoluble Ash: <span id="acidInsolubleAshSpan">Nil</span></li>
                                        <li> Mold % by Weight: <span id="moldWeightSpan">Nil</span></li>
                                        <li> Whole Dead Insects: <span id="wholeDeadInsectsSpan">Nil</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Additional Information</h4>
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>
                                        <li>File: <span id="fileUploadSpan">Nil</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Quantity Specification</h4>
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>
                                        <li>Quantity: <span id="QuantitySpan">Nil</span></li>
                                        <li>Demand Price: <span id="demandPriceSpan">Nil</span></li>
                                        {{-- <li>Preferred Currency: <span id="preferredCurrencySpan">Nil</span></li> --}}
                                        <li> Preferred Delivery Location: <span id="preferredLocationSpan">Nil</span></li>
                                        <li> Preferred Volume Unit: <span id="preferredUnitSpan">Nil</span></li>
                                        <li> Delivery Date Deadline: <span id="deliveryDateSpan">Nil</span></li>
                                        <li> Preferred Delivery Location Country: <span
                                                id="preferredCountrySpan">Nil</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Agreement</h4>
                                <div class="underline"></div>
                                <div class="output_container terms_preview">
                                    <ul>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path dataMarker="check1" d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I agree that all the information I have provided are correct and true to my best
                                            knowledge.
                                        </li>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path dataMarker="check2" d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I agree that the company that I am affiliated with has sincere intention of actually purchasing this products from suitable suppliers without any
                                            intention to purely check suppliers’ prices or benchmark competitors’ prices without the intent to purchase.
                                        </li>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path dataMarker="check3" d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I agree to provide proper feedback on any offers proposed to me by any of the suppliers. I also agree to respond to any of the supplier’s in a timely manner.
                                        </li>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path dataMarker="check4" d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I understand that, any company can be penalized or permanently blocked on Farmcrowdy Traders’ platform if any points agreed here turns out to be false.
                                        </li>

                                    </ul>
                                </div>
                                <div class="btn_groups">
                                    <div class="prev next_btn">Previous Step</div>
                                    <button class="next_btn">Submit</button>
                                    <!-- <div class="button next_btn">Next Step</div> -->
                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        <div>
    </main>
@endsection
@section('scripts')
<script src="{{ asset('js/fileUploader.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
@endsection('scripts')
