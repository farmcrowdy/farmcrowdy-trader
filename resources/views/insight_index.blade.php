@extends('layouts.fctrader')

@section('head_scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('css/fcTrader.min.css') }}">
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{-- <link rel="stylesheet" href="{{ asset('css/traderMsg.min.css') }}"> --}}
@endsection('head_scripts')

@section('nav')
@include('fctrader.nav')
@endsection('nav')
@section('content')
    <main>
        @include('fctrader.page_header')
        <div class="main_body">
            <!-- <div class="chart_display">
                <h4 class="chart_title">Crop Performance Graph</h4>
                <div class="underline"></div>
                <div class="chart_">
                    <div class="chart_select_options">
                        <div class="form_container">
                            <label for="chartCrop">Crop:</label>
                            <select name="chartCrop" id="chartCrop" class="chartOption">
                                <option value="Maize">Maize</option>
                                <option value="Rice">Rice</option>
                                <option value="Beans">Beans</option>
                            </select>
                        </div> --}}
                        <div class="form_container">
                            <label for="chartCountry">Country:</label>
                            <select name="chartCountry" id="chartCountry" class="chartOption">
                                <option value="all">All</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Ghana">Ghana</option>
                            </select>
                        </div>
                        <div class="form_container">
                            <label for="chartCurrency">Currency:</label>
                            <select name="chartCurrency" id="chartCurrency" class="chartOption">
                                <option value="USD">USD</option>
                            </select>
                        </div>
                        <div class="form_container">
                            <label for="chartTime">Time period:</label>
                            <select name="chartTime" id="chartTime" class="chartOption">
                                <option value="year">Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="chart_container">
                        <canvas id="myChart" width="400" height="400"></canvas>
                    </div> 
                </div>
            </div>
            -->
        
            <section class="insight_table_stat">
                <div class="payment_table" id="insightTable">
                    <div class="table_container">
                        <table id="insightDataTable" class="table dt-responsive" width="100%">
                            <thead>
                                <tr>
                                    <th>Commodity</th>
                                    <th>Last Week’s Price</th>
                                    <th>This Week’s Price</th>
                                    <th>% Change</th>                               
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        Fresh Ginger
                                    </td>
                                    <td>
                                        5,500
                                    </td>
                                    <td class="">
                                        6,600
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>22%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        Dried Split Ginger
                                    </td>
                                    <td>
                                        792,000
                                    </td>
                                    <td class="">
                                        902,000
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>12.20%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        Rice Paddy
                                    </td>
                                    <td>
                                        184,800
                                    </td>
                                    <td class="">
                                        192,500
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>4%</span>
                                    </td>
                                  
                                </tr>
                                <tr>
                                    <td>
                                        Soya Beans
                                    </td>
                                    <td>
                                        181,500
                                    </td>
                                    <td class="">
                                        203,500
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>10.81%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        Plantain
                                    </td>
                                    <td>
                                        6,600
                                    </td>
                                    <td class="">
                                        7,700
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>14.29%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        Maize
                                    </td>
                                    <td>
                                        119,900
                                    </td>
                                    <td class="">
                                        123,200
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>2.68%</span>
                                        
                                    </td>
                                  
                                </tr>
                                <tr>
                                    <td>
                                        Shearnut
                                    </td>
                                    <td>
                                        90,200
                                    </td>
                                    <td class="">
                                        82,500
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent _down">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>9.33%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        Cowpea
                                    </td>
                                    <td>
                                        198,000
                                    </td>
                                    <td class="">
                                        198,000
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>0.00%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        Sorghum
                                    </td>
                                    <td>
                                        165,000
                                    </td>
                                    <td class="">
                                        165,000
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>0.00%</span>
                                        
                                    </td>
                                
                                </tr>
                                <tr>
                                    <td>
                                        Groundnut
                                    </td>
                                    <td>
                                        209,000
                                    </td>
                                    <td class="">
                                        209,000
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>0.00%</span>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td>
                                        White beans
                                    </td>
                                    <td>
                                        324,500
                                    </td>
                                    <td class="">
                                        324,500
                                    </td>
                                    <!-- add class "_down" to <td> when the price is down -->
                                    <td class="percent ">
                                        <span class="down_icon">-</span>
                                        <span class="up_icon">+</span> 
                                        <span>0.00%</span>
                                    </td>
                                   
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <section class="price_table_group">
                <!-- first insight  -->
            <!-- <div class="price_table insight_group">
                <div class="price_table_header">
                    <div class="location_">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.6626 22.3295C22.6626 22.3295 23.0035 22.0739 22.3643 22.3295C21.7251 22.5852 21.2137 22.9261 21.2137 22.9261V23.4423V23.9489C21.2137 23.9489 20.4467 24.2045 21.2137 24.6307C21.2137 24.6307 21.3842 24.7159 21.7251 24.6307C22.066 24.5455 22.066 24.5455 22.066 24.5455C22.066 24.5455 22.3217 24.2898 22.833 23.9489C23.3444 23.608 23.5148 23.1818 23.5148 23.1818L23.0887 22.3295H22.6626Z" fill="#009CDE"/>
                            <path d="M15 0C6.72886 0 0 6.72886 0 15C0 23.2711 6.72886 30 15 30C23.2711 30 30 23.2711 30 15C30 6.72886 23.2711 0 15 0ZM27.691 19.6875C27.3501 20.0284 27.2649 20.1136 26.6683 20.3693C26.6683 20.3693 26.3274 20.9659 25.9012 21.2216C25.4751 21.4773 25.9012 21.6477 25.9012 21.6477V22.216L25.4751 22.7557L25.049 23.4423L24.6228 24.2045L23.941 24.6685V25.0568V25.6534C23.941 25.6534 23.9195 25.7035 23.8868 25.7809C21.4694 27.7769 18.3723 28.9773 15 28.9773C11.2735 28.9773 7.88591 27.5086 5.3775 25.1233L5.27625 24.8864L5.36148 24.4602H5.95807V24.0341V23.6932L5.53193 23.0966V22.7557V22.4148V22.1591V21.0511V20.5398V20.1989H5.02057H4.25352L3.5717 19.6875L2.97511 19.1761V18.8352H1.95239V18.1534V17.5568H1.3558L1.2375 17.4239C1.09909 16.636 1.02273 15.827 1.02273 15C1.02273 13.0756 1.41375 11.2408 2.12045 9.57068C2.27795 9.50318 2.37852 9.46023 2.37852 9.46023H2.71943C2.71943 9.46023 2.97511 9.71591 3.2308 9.46023C3.48648 9.20455 3.91261 9.46023 3.91261 9.46023C3.91261 9.46023 4.1683 9.57375 4.42398 9.3467C4.67966 9.11966 4.85011 8.94886 4.93534 8.69318C5.02057 8.4375 5.02057 8.4375 5.02057 8.4375C5.02057 8.4375 4.42398 7.84091 5.02057 7.67045C5.61716 7.5 5.74023 7.41477 6.14761 7.24432C6.555 7.07386 6.55466 7.07386 6.55466 7.07386H7.23648V6.5625V6.05114V5.28409L7.74784 4.77273C7.74784 4.77273 8.08875 4.60227 7.74784 4.34659C7.51295 4.17 7.15943 3.795 6.96068 3.57682C9.23659 1.97045 12.0089 1.02273 15 1.02273C17.548 1.02273 19.936 1.71136 21.9955 2.90693L21.3842 3.57955L20.7876 4.09091H20.3615C20.3615 4.09091 20.1058 4.26136 19.7649 4.09091C19.424 3.92045 18.9126 3.49432 18.9126 3.49432L18.5717 2.93557C18.5717 2.93557 18.2308 2.88818 17.9751 2.93557C17.7194 2.98295 17.2933 2.93557 17.2933 2.93557H16.7819H16.1853L14.2251 3.32386C14.0547 3.75 14.7365 4.51705 14.2251 4.94318C13.7138 5.36932 13.7138 5.36932 13.7138 5.36932V6.13636L13.2876 6.5625V7.07386L12.691 7.58523L12.0944 6.98864C12.0944 6.98864 12.3501 6.64773 12.0944 6.47727C11.8388 6.30682 11.8388 6.30682 11.8388 6.30682L10.816 6.05114C10.816 6.05114 10.3047 6.39205 10.049 6.5625C9.7933 6.73295 9.7933 6.73295 9.7933 6.73295L10.2194 7.58523H10.6456H11.1569L11.4126 8.18182L10.9013 8.69318C10.9013 8.69318 10.9865 8.35227 10.3047 8.69318C9.62284 9.03409 9.62284 9.03409 9.62284 9.03409C9.62284 9.03409 9.28193 9.54545 9.62284 9.80114C9.96375 10.0568 10.5603 10.7956 10.5603 10.7956L9.02625 11.7614C8.51489 12.2727 8.51489 12.6136 8.34443 13.6364C8.17398 14.6591 7.96568 14.5739 7.8992 15C7.83273 15.4261 7.8992 15.9375 7.8992 15.9375L7.86955 16.4489C7.86955 16.4489 7.28352 16.4489 7.8992 16.875C8.43204 17.2439 8.9492 17.6755 9.07875 17.7849C9.10023 17.7706 9.22909 17.7644 9.70807 17.8125C10.5603 17.8977 12.0944 17.8125 12.0944 17.8125L13.1172 18.9205L13.6285 20.0284L13.9694 20.4545C13.9694 20.4545 14.3956 20.8807 14.4808 21.1364C14.566 21.392 15.0774 21.733 15.0774 21.733V22.1591C15.0774 22.1591 14.8217 22.7557 14.8217 23.0114C14.8217 23.267 14.8217 23.267 14.8217 23.267L15.524 24.2898V25.0568C15.524 25.0568 15.374 25.3125 15.524 25.5682C15.674 25.8239 15.674 25.8239 15.674 25.8239C15.674 25.8239 15.7592 26.25 15.9297 26.5057C16.1001 26.7614 16.1001 26.7614 16.1001 26.7614C16.1001 26.7614 16.3558 27.4432 16.6115 27.1875C16.8672 26.9318 16.8672 26.9318 16.8672 26.9318L17.6342 25.9091L18.0603 25.8239L19.0831 25.3977L19.5944 24.6307L20.1058 23.8636L20.191 23.3523L20.3615 22.5852C20.3615 22.5852 20.7876 22.2443 20.9581 21.9886C21.1285 21.733 21.1285 21.5625 21.299 21.3068C21.4694 21.0511 21.8103 20.5398 21.8103 20.5398L22.1512 19.9432L22.7478 19.3449C22.7478 19.3449 23.5149 18.75 23.4297 18.4943C23.3444 18.2386 23.4365 17.8125 23.4365 17.8125L24.1115 17.3011V16.3636H23.4297L22.8331 16.1932C22.8331 16.1932 22.5774 15.8523 22.3217 15.8523C22.066 15.8523 22.7478 16.3636 22.066 15.8523C21.3842 15.3409 21.3842 15.3409 21.3842 15.3409L20.7876 15L20.2763 14.0625C20.2763 14.0625 20.7024 13.892 20.191 13.5511C19.6797 13.2102 20.1058 13.8068 19.6797 13.2102C19.2535 12.6136 19.2535 12.6136 19.2535 12.6136L18.4013 11.7614H17.8899H17.4638H16.9524H16.6115C16.6115 11.7614 16.6115 12.1023 16.1853 11.7614C15.8492 11.4924 15.1432 11.2769 14.8595 11.1978C14.8238 11.1982 14.7365 11.1648 14.7365 11.1648C14.7365 11.1648 14.7839 11.1767 14.8595 11.1978C14.9103 11.1972 14.8592 11.128 14.3103 10.7952C13.3728 10.2269 13.3728 10.2269 13.3728 10.2269L12.7763 10.1608C12.7763 10.1608 12.4353 9.8008 12.7763 9.54511C13.1172 9.28943 13.1172 9.28943 13.1172 9.28943L13.799 9.11898L14.3103 8.94852L14.7365 8.52239L16.2706 9.11898L16.6967 9.54511L17.2081 9.8008L17.8047 10.1608C17.8047 10.1608 17.8899 9.8008 18.2308 9.8008C18.5717 9.8008 18.5717 9.8008 18.5717 9.8008L19.2535 10.7952H19.5944L19.9353 11.2497L19.8501 11.761V12.1872C19.8501 12.1872 20.1058 12.1872 20.3615 12.6133C20.6172 13.0394 20.6172 13.0394 20.6172 13.0394C20.6172 13.0394 20.7024 13.2951 21.0433 13.636C21.3842 13.9769 21.3842 13.9769 21.3842 13.9769C21.3842 13.9769 21.6399 14.1194 21.8956 14.4743C22.1512 14.8292 22.1512 14.8295 22.1512 14.8295C22.1512 14.8295 22.2365 14.4034 22.5774 15C22.9183 15.5966 22.9183 15.5966 22.9183 15.5966C22.9183 15.5966 23.2592 15.6818 23.5149 15.6818C23.7706 15.6818 24.1115 15.5114 24.1115 15.5114C24.1115 15.5114 24.3672 15.1705 24.6228 15C24.8785 14.8295 25.049 14.4747 25.049 14.4747V13.8068L25.5603 13.4659L25.9865 13.2955C25.9865 13.2955 26.4126 13.2102 26.5831 13.4659C26.7535 13.7216 26.7535 13.7216 26.7535 13.7216C26.7535 13.7216 27.0092 14.1198 27.0944 14.4747C27.1797 14.8295 27.1797 14.8295 27.1797 14.8295V15.6818V16.0227L27.691 16.4489C27.691 16.4489 26.924 17.2159 27.691 16.7898C28.4581 16.3636 28.3728 17.1307 28.4581 16.3636C28.5215 15.7936 28.8201 15.0283 28.9691 14.6724C28.9718 14.7815 28.9773 14.8902 28.9773 15C28.9773 16.4137 28.7645 17.7784 28.3725 19.0657C28.1932 19.2198 27.9078 19.4703 27.691 19.6875Z" fill="#009CDE"/>
                        </svg>
                        <h6>Latest Research Insight</h6>                                
                    </div>
                    <div class="more_link">
                        <a href="#">See more</a>
                    </div>
                </div> 
                <div class="post_groups">
                    <a href="#" class="post_item">
                    <div class="img_con">
                        <img src="{{asset('images/post.jpg')}}" alt="Post image">
                    </div>
                        <div class="post_body">
                            <p class="crop_type">Maize</p>
                            <h5>‘Wild’ genes open up opportunities for healthier, climate-smart rice</h5>
                            <p class="post_detail">The genome sequencing of seven wild rice varieties has finally been completed. This breakthrough is expected to provide opportunities for breeders</p>
                            <div class="post_footer">
                                <p>18 Jan, 2018 <span>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.6551 2.34833C12.1436 0.834112 10.1353 0 8 0C5.86436 0 3.85564 0.834112 2.34347 2.34833C0.832178 3.86202 0 5.87297 0 8.01175C0 10.1292 0.854578 12.1853 2.34347 13.6517C3.856 15.1659 5.86507 16 8 16C8.25422 16 8.50667 15.9874 8.7568 15.9639C8.76907 15.9628 8.78151 15.9623 8.79378 15.9612C9.05387 15.9357 9.31129 15.8974 9.56533 15.8471C9.56729 15.8467 9.56942 15.8464 9.57138 15.846C11.1077 15.5405 12.5209 14.7876 13.6539 13.6526L13.654 13.6524C13.6542 13.6522 13.6542 13.6522 13.6542 13.6522C14.9349 12.3917 15.7444 10.6968 15.9477 8.90177C15.9515 8.8683 15.957 8.83518 15.9604 8.80171C15.9867 8.54017 16 8.27649 16 8.01175C16 5.87297 15.1671 3.86166 13.6551 2.34833ZM12.472 11.6493L7.61724 8.83661C7.34222 8.72195 7.16444 8.44367 7.16444 8.12748V3.17319C7.16444 2.73575 7.50667 2.39284 7.94364 2.39284C8.35769 2.39284 8.6944 2.74269 8.6944 3.17319V7.5898C8.6944 7.65496 8.72889 7.71478 8.78507 7.74754L13.2226 10.3231C13.2233 10.3234 13.224 10.3238 13.2247 10.3241C13.5929 10.5317 13.7228 10.9943 13.5221 11.3788C13.5035 11.4119 13.4805 11.442 13.4571 11.4714C13.451 11.4791 13.4466 11.488 13.4402 11.4953C13.4158 11.5234 13.3877 11.5478 13.3593 11.5718C13.3515 11.5784 13.3452 11.5864 13.3371 11.5926C13.262 11.6507 13.1748 11.6945 13.0789 11.7214C12.8759 11.778 12.654 11.7516 12.472 11.6493Z" fill="#A9B2B2"/>
                                    </svg>
                                    5 min read </span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="post_item">
                        <div class="img_con">
                            <img src="{{asset('images/post.jpg')}}" alt="Post image">
                        </div>
                        <div class="post_body">
                            <p class="crop_type">All</p>
                            <h5>The Indoor AgTech Innovation Summit is going virtual!</h5>
                            <p class="post_detail">The Indoor AgTech Innovation Summit is going virtual!  This year’s summit will be live online on July 23, 2020, providing an essential opportunity for the industry to meet, network and exchange ideas at this critical time for our industr....</p>
                            <div class="post_footer">
                                <p>30 June, 2020 <span>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.6551 2.34833C12.1436 0.834112 10.1353 0 8 0C5.86436 0 3.85564 0.834112 2.34347 2.34833C0.832178 3.86202 0 5.87297 0 8.01175C0 10.1292 0.854578 12.1853 2.34347 13.6517C3.856 15.1659 5.86507 16 8 16C8.25422 16 8.50667 15.9874 8.7568 15.9639C8.76907 15.9628 8.78151 15.9623 8.79378 15.9612C9.05387 15.9357 9.31129 15.8974 9.56533 15.8471C9.56729 15.8467 9.56942 15.8464 9.57138 15.846C11.1077 15.5405 12.5209 14.7876 13.6539 13.6526L13.654 13.6524C13.6542 13.6522 13.6542 13.6522 13.6542 13.6522C14.9349 12.3917 15.7444 10.6968 15.9477 8.90177C15.9515 8.8683 15.957 8.83518 15.9604 8.80171C15.9867 8.54017 16 8.27649 16 8.01175C16 5.87297 15.1671 3.86166 13.6551 2.34833ZM12.472 11.6493L7.61724 8.83661C7.34222 8.72195 7.16444 8.44367 7.16444 8.12748V3.17319C7.16444 2.73575 7.50667 2.39284 7.94364 2.39284C8.35769 2.39284 8.6944 2.74269 8.6944 3.17319V7.5898C8.6944 7.65496 8.72889 7.71478 8.78507 7.74754L13.2226 10.3231C13.2233 10.3234 13.224 10.3238 13.2247 10.3241C13.5929 10.5317 13.7228 10.9943 13.5221 11.3788C13.5035 11.4119 13.4805 11.442 13.4571 11.4714C13.451 11.4791 13.4466 11.488 13.4402 11.4953C13.4158 11.5234 13.3877 11.5478 13.3593 11.5718C13.3515 11.5784 13.3452 11.5864 13.3371 11.5926C13.262 11.6507 13.1748 11.6945 13.0789 11.7214C12.8759 11.778 12.654 11.7516 12.472 11.6493Z" fill="#A9B2B2"/>
                                    </svg>
                                        5 min read </span></p>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="post_item">
                        <div class="img_con">
                            <img src="images/maize.png" alt="Post image">
                        </div>
                        <div class="post_body">
                            <p class="crop_type">Maize</p>
                            <h5>6 Agtech startups selected for innovation Incubator</h5>
                            <p class="post_detail">Six early-stage companies have been selected to participate in the Wells Fargo Innovation Incubator, a technology incubator and platform. Each company 
                            participating in the second cohort of this 
                            invitation-only program will receive up to $250,000....</p>
                            <div class="post_footer">
                                <p>27 May, 2020 <span>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M13.6551 2.34833C12.1436 0.834112 10.1353 0 8 0C5.86436 0 3.85564 0.834112 2.34347 2.34833C0.832178 3.86202 0 5.87297 0 8.01175C0 10.1292 0.854578 12.1853 2.34347 13.6517C3.856 15.1659 5.86507 16 8 16C8.25422 16 8.50667 15.9874 8.7568 15.9639C8.76907 15.9628 8.78151 15.9623 8.79378 15.9612C9.05387 15.9357 9.31129 15.8974 9.56533 15.8471C9.56729 15.8467 9.56942 15.8464 9.57138 15.846C11.1077 15.5405 12.5209 14.7876 13.6539 13.6526L13.654 13.6524C13.6542 13.6522 13.6542 13.6522 13.6542 13.6522C14.9349 12.3917 15.7444 10.6968 15.9477 8.90177C15.9515 8.8683 15.957 8.83518 15.9604 8.80171C15.9867 8.54017 16 8.27649 16 8.01175C16 5.87297 15.1671 3.86166 13.6551 2.34833ZM12.472 11.6493L7.61724 8.83661C7.34222 8.72195 7.16444 8.44367 7.16444 8.12748V3.17319C7.16444 2.73575 7.50667 2.39284 7.94364 2.39284C8.35769 2.39284 8.6944 2.74269 8.6944 3.17319V7.5898C8.6944 7.65496 8.72889 7.71478 8.78507 7.74754L13.2226 10.3231C13.2233 10.3234 13.224 10.3238 13.2247 10.3241C13.5929 10.5317 13.7228 10.9943 13.5221 11.3788C13.5035 11.4119 13.4805 11.442 13.4571 11.4714C13.451 11.4791 13.4466 11.488 13.4402 11.4953C13.4158 11.5234 13.3877 11.5478 13.3593 11.5718C13.3515 11.5784 13.3452 11.5864 13.3371 11.5926C13.262 11.6507 13.1748 11.6945 13.0789 11.7214C12.8759 11.778 12.654 11.7516 12.472 11.6493Z" fill="#A9B2B2"/>
                                    </svg>
                                        5 min read </span></p>
                            </div>        
                        </div>
                    </a>
                </div>
            </div>
            <div class="price_table">
                    <div class="price_table_header">
                        <div class="location_">
                            <svg width="30" height="20" viewBox="0 0 30 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0V20H30V0H0ZM10.0697 0.526316H19.8955V19.4737H10.0697V0.526316Z" fill="#008751"/>
                                <rect x="10.1045" width="9.7561" height="20" fill="white"/>
                            </svg>                            
                            <h6>Latest Research Insight</h6>                                
                        </div>
                        <div class="more_link">
                            <a href="#">See more</a>
                        </div>
                    </div>
                    <div class="post_groups">
                        <a href="#" class="post_item">
                            <img src="{{asset('images/post.jpg')}}" alt="Post image">
                            <h5>Agric value chains must go digital to drive growth – Ademola</h5>
                            <p>“We believe that we are on the right path, especially with the inclusion of technology in terms of digital agriculture,” he said.
                            ....</p>
                        </a>
                        <a href="#" class="post_item">
                            <img src="{{asset('images/post.jpg')}}" alt="Post image">
                            <h5>Why Nigeria must bridge infrastructural gaps to aid food production </h5>
                            <p>As Nigeria makes efforts to ensure that agriculture plays a key role in its quest for revenue 
                            diversification, stakeholders in the sector have charged the Federal Government to bridge....</p>
                        </a>
                        <a href="#" class="post_item">
                            <img src="{{asset('images/post.jpg')}}" alt="Post image">
                            <h5>Boosting rubber production will create 68,000 jobs – Igbinosun</h5>
                            <p>Peter Igbinosun, national president of Rubber Producers, Processors and Marketers Association of Nigeria (NARPPMAN) has said that the country can generate 68,000 jobs from rubber production....</p>
                        </a>
                    </div>
                </div>
            </section> -->
        </div>
    </main>
@endsection()
@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/charts.js') }}"></script>
<script>
$(document).ready(function(){
    // insight data table 
    let insightTable = document.getElementById('insightTable');
    if(insightTable){
        $('#insightDataTable').DataTable(); 
    }
});
</script>
@endsection('scripts')
