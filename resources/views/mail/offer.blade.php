<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Offer Mail</title>
</head>
<body>
  <p>An offer has been created by {{ $offer->user->first_name.' '.$offer->user->last_name }}</p>
  <p>Product: {{ $offer->product->name }}</p>
  <p>Quantity: {{ $offer->quantity }}</p>
  <p>Offer Price: {{ $offer->offer_price }}</p>
</body>
</html>
