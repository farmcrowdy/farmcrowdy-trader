<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Order Mail</title>
</head>
<body>
  <p>An order has been created by {{ $order->user->first_name.' '.$order->user->last_name }}</p>
  <p>Product: {{ $order->product->name }}</p>
  <p>Quantity: {{ $order->quantity }}</p>
  <p>Demand Price: {{ $order->demand_price }}</p>
</body>
</html>
