@extends('layouts.fctrader')
@section('head_scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
@endsection('head_scripts')
@section('nav')
@include('fctrader.nav')
@endsection('nav')
@section('content')
    <main>
        @include('fctrader.page_header')
        <div class="main_body">
        @if (session('profile_updated'))
            <div class="ml-4 mr-4 alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> {{ session('profile_updated') }}
            </div>
        @endif
        @if($errors->any())
        <div class="ml-4 mr-4 alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Error!</strong> Please check the fields marked in red.
        </div>
        @endif
            <section class="profile_transaction">
                <div class="section_nav">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        {{-- <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="transactions-tab" data-toggle="tab" href="#transactions"
                                role="tab" aria-controls="transactions" aria-selected="true">Transactions</a>
                        </li> --}}
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="userProfile-tab" data-toggle="tab" href="#userProfile" role="tab"
                                aria-controls="userProfile" aria-selected="true">User Profile</a>
                        </li>

                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    {{-- <div class="tab-pane fade show active" id="transactions" role="tabpanel"
                        aria-labelledby="transactions-tab">
                        <div class="dash_stat">
                            <div class="stat_item">
                                <div class="details">
                                    <div class="price">
                                        <p>$<span>300,000</span></p>
                                        <p class="p_">Total Transaction Amount</p>
                                    </div>
                                    <svg width="72" height="72" viewBox="0 0 72 72" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="36" cy="36" r="36" fill="#E2F6B6" fill-opacity="0.7" />
                                        <circle cx="36" cy="36" r="28" fill="white" />
                                        <path
                                            d="M55.3333 49H21.1109C20.7553 49 20.4442 48.6779 20.4442 48.3097C20.4442 47.9416 20.7553 47.6195 21.1109 47.6195H54.6666V28.292C54.6666 27.9239 54.9777 27.6017 55.3333 27.6017C55.6889 27.6017 56 27.9239 56 28.292V48.3097C56 48.7239 55.6889 49 55.3333 49Z"
                                            fill="#86BA16" />
                                        <path
                                            d="M53.1112 46.6991H18.8888C18.5332 46.6991 18.2221 46.377 18.2221 46.0088C18.2221 45.6407 18.5332 45.3186 18.8888 45.3186H52.4445V25.9911C52.4445 25.623 52.7556 25.3008 53.1112 25.3008C53.4668 25.3008 53.7779 25.623 53.7779 25.9911V46.0088C53.7779 46.423 53.4668 46.6991 53.1112 46.6991Z"
                                            fill="#86BA16" />
                                        <path
                                            d="M50.8891 44.3983H16.6667C16.3111 44.3983 16 44.0761 16 43.708V23.6903C16 23.3221 16.3111 23 16.6667 23H50.8891C51.2446 23 51.5558 23.3221 51.5558 23.6903V43.708C51.5558 44.1222 51.2446 44.3983 50.8891 44.3983ZM17.3333 43.0177H50.2224V24.4266H17.3333V43.0177Z"
                                            fill="#86BA16" />
                                        <path
                                            d="M24.6667 33.7221C24.6667 30.915 25.8222 28.4301 27.6889 26.7274H20.2222C19.8667 26.7274 19.5555 27.0495 19.5555 27.4177V40.0726C19.5555 40.4407 19.8667 40.7628 20.2222 40.7628H27.6889C25.8222 39.0142 24.6667 36.5292 24.6667 33.7221Z"
                                            fill="#86BA16" />
                                        <path
                                            d="M47.3335 26.7274H39.8668C41.7335 28.4761 42.8891 30.961 42.8891 33.7221C42.8891 36.4832 41.7335 39.0142 39.8668 40.7168H47.3335C47.6891 40.7168 48.0002 40.3947 48.0002 40.0266V27.3717C48.0002 27.0035 47.6891 26.7274 47.3335 26.7274Z"
                                            fill="#86BA16" />
                                        <path
                                            d="M33.3444 28.1854C32.8093 29.8215 32.7185 31.6936 32.6078 33.3994C32.4875 35.2527 32.4269 37.1018 32.669 38.9467C32.746 39.5336 33.5861 39.5723 33.6133 38.9467C33.6915 37.1616 33.6221 35.3769 33.6643 33.5906C33.706 31.8246 33.9612 30.0165 33.8577 28.2547C33.8409 27.9605 33.4379 27.9 33.3444 28.1854Z"
                                            fill="#86BA16" />
                                        <path
                                            d="M29.0639 35.4604C29.8259 36.3503 30.5257 37.1555 31.167 38.1488C31.6118 38.8379 32.2048 39.9374 33.1356 39.9979C34.2006 40.0668 34.9162 38.4223 35.4532 37.7397C36.2887 36.6782 37.3288 35.7461 38.0784 34.626C38.4029 34.1411 37.7521 33.4776 37.2652 33.8128C36.345 34.4456 35.6314 35.355 34.9284 36.2135C34.5905 36.6257 34.2622 37.0463 33.9424 37.473C33.7528 37.7263 33.544 38.1208 33.3153 38.3376C33.0376 38.6012 33.2122 38.6606 32.8732 38.4189C32.1737 37.9205 31.7056 36.8873 31.1398 36.2326C30.6364 35.6496 30.0342 35.1095 29.2459 35.0217C29.0034 34.9942 28.9352 35.3102 29.0639 35.4604Z"
                                            fill="#86BA16" />
                                    </svg>
                                </div>
                            </div>
                            <div class="stat_item">
                                <div class="details">
                                    <div class="price">
                                        <p><span>300,000</span>MT</p>
                                        <p class="p_">Total Transaction Volume</p>
                                    </div>
                                    <svg width="72" height="72" viewBox="0 0 72 72" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="36" cy="36" r="36" fill="#E2F6B6" fill-opacity="0.7" />
                                        <circle cx="36" cy="36" r="28" fill="white" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M47.2216 30.7874H39.2849L39.2047 30.3697C41.1006 29.5218 42.4269 27.6336 42.4269 25.4249C42.4269 22.43 39.9924 20 37 20C34.008 20 31.5738 22.4398 31.5738 25.4347C31.5738 27.6437 32.9001 29.5205 34.7957 30.3684L34.7158 30.7874H26.7784L22 52H52L47.2216 30.7874ZM33.7869 25.4379C33.7869 23.6616 35.2257 22.2214 37 22.2214C38.7753 22.2214 40.2134 23.6619 40.2134 25.4379C40.2134 26.5645 39.6341 27.5546 38.7577 28.129C38.4352 27.7325 37.773 27.4577 36.9997 27.4577C36.227 27.4577 35.5648 27.7325 35.2423 28.129C34.3666 27.5543 33.7869 26.5645 33.7869 25.4379Z"
                                            fill="#86BA16" />
                                    </svg>
                                </div>
                            </div>
                            <div class="stat_item">
                                <div class="details">
                                    <div class="price">
                                        <p><span>300</span></p>
                                        <p class="p_">Completed Offers</p>
                                    </div>
                                    <svg width="72" height="72" viewBox="0 0 72 72" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="36" cy="36" r="36" fill="#E2F6B6" fill-opacity="0.7" />
                                        <circle cx="36" cy="36" r="28" fill="white" />
                                        <path
                                            d="M34.2092 21.321C33.936 21.1586 33.6091 21.1101 33.2999 21.1862C32.9908 21.2622 32.7246 21.4567 32.5598 21.7269L27.8524 29.5108H20.9779C20.5182 29.5057 20.0642 29.6117 19.6551 29.8197C19.2461 30.0277 18.8943 30.3315 18.6302 30.7046C18.3053 31.1832 18.0989 31.731 18.0277 32.3035C17.9566 32.8761 18.0228 33.4572 18.2209 33.9996L22.844 48.7794C23.088 49.4325 23.5282 49.9956 24.1052 50.3927C24.6822 50.7898 25.3682 51.0018 26.0705 50.9999H45.9355C46.6399 51.0042 47.3286 50.7934 47.908 50.3961C48.4874 49.9989 48.9294 49.4344 49.1741 48.7794L53.7972 33.9399C53.9837 33.405 54.0427 32.8345 53.9696 32.2732C53.8964 31.7119 53.6932 31.175 53.3759 30.7046C53.1118 30.3315 52.76 30.0277 52.3509 29.8197C51.9419 29.6117 51.4879 29.5057 51.0282 29.5108H44.1537L39.4463 21.7389C39.3807 21.5811 39.2818 21.4391 39.1561 21.3224C39.0305 21.2058 38.8811 21.1172 38.718 21.0626C38.5549 21.0081 38.3819 20.9888 38.2107 21.0062C38.0395 21.0236 37.874 21.0771 37.7254 21.1633C37.5769 21.2494 37.4487 21.3662 37.3494 21.5056C37.2502 21.6451 37.1823 21.804 37.1503 21.9717C37.1183 22.1394 37.1229 22.312 37.1638 22.4777C37.2047 22.6435 37.281 22.7987 37.3876 22.9327L41.3365 29.5108H30.6696L34.6305 22.9566C34.7933 22.6843 34.8404 22.359 34.7614 22.0523C34.6824 21.7456 34.4837 21.4825 34.2092 21.321ZM42.7812 31.8985L44.5991 34.895C44.6647 35.0528 44.7637 35.1948 44.8893 35.3115C45.015 35.4281 45.1643 35.5167 45.3274 35.5713C45.4905 35.6258 45.6635 35.6451 45.8347 35.6277C46.0059 35.6103 46.1714 35.5568 46.32 35.4706C46.4686 35.3845 46.5968 35.2677 46.696 35.1283C46.7952 34.9888 46.8631 34.8299 46.8952 34.6622C46.9272 34.4945 46.9226 34.3219 46.8816 34.1561C46.8407 33.9904 46.7644 33.8352 46.6579 33.7012L45.6345 31.8985H51.0763C51.1533 31.8885 51.2316 31.8987 51.3034 31.9281C51.3752 31.9574 51.4381 32.0048 51.4857 32.0656C51.5772 32.2515 51.6247 32.4557 51.6247 32.6625C51.6247 32.8694 51.5772 33.0735 51.4857 33.2595L46.8385 48.075C46.7433 48.2385 46.6064 48.3742 46.4416 48.4685C46.2767 48.5628 46.0897 48.6124 45.8994 48.6123H26.0705C25.8864 48.6062 25.7068 48.5537 25.5488 48.4597C25.3908 48.3657 25.2596 48.2333 25.1676 48.075L20.5204 33.2356C20.4289 33.0497 20.3813 32.8455 20.3813 32.6387C20.3813 32.4318 20.4289 32.2276 20.5204 32.0417C20.5749 31.9771 20.647 31.9292 20.728 31.9038C20.8091 31.8784 20.8958 31.8766 20.9779 31.8985H26.3715L25.3 33.6773C25.1893 33.9437 25.1795 34.2408 25.2723 34.5138C25.3652 34.7869 25.5545 35.0174 25.8053 35.1628C26.0562 35.3083 26.3516 35.3589 26.6371 35.3053C26.9226 35.2516 27.1789 35.0975 27.3588 34.8711L29.2249 31.8985H42.7812Z"
                                            fill="#85BA16" />
                                        <path
                                            d="M28.7791 46.2246H43.2264C43.5457 46.2246 43.8519 46.0988 44.0777 45.8749C44.3035 45.651 44.4303 45.3474 44.4303 45.0308C44.4303 44.7141 44.3035 44.4105 44.0777 44.1866C43.8519 43.9627 43.5457 43.8369 43.2264 43.8369H28.7791C28.4598 43.8369 28.1536 43.9627 27.9278 44.1866C27.702 44.4105 27.5752 44.7141 27.5752 45.0308C27.5752 45.3474 27.702 45.651 27.9278 45.8749C28.1536 46.0988 28.4598 46.2246 28.7791 46.2246Z"
                                            fill="#85BA16" />
                                        <path
                                            d="M28.7791 41.4492H43.2264C43.5457 41.4492 43.8519 41.3234 44.0777 41.0995C44.3035 40.8757 44.4303 40.572 44.4303 40.2554C44.4303 39.9387 44.3035 39.6351 44.0777 39.4112C43.8519 39.1873 43.5457 39.0615 43.2264 39.0615H28.7791C28.4598 39.0615 28.1536 39.1873 27.9278 39.4112C27.702 39.6351 27.5752 39.9387 27.5752 40.2554C27.5752 40.572 27.702 40.8757 27.9278 41.0995C28.1536 41.3234 28.4598 41.4492 28.7791 41.4492Z"
                                            fill="#85BA16" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="chart_insight chart_display" id="transaction_chart">
                            <div class="chart_1">
                                <h4 class="chart_title">Track Record - <span>Quantity vs Date</span></h4>
                                <div class="underline"></div>
                                <div class="chart_">
                                    <div class="chart_select_options">
                                        <div class="form_container">
                                            <label for="chartCrop">Crop:</label>
                                            <select name="chartCrop" id="chartCrop" class="chartOchartOption1ption">
                                            @foreach ($products as $product)
                                            <option value="{{$product->id}}" @if( old('product_id', $user->product_id)  == $product->id) selected="selected" @endif>{{$product->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>

                                        <div class="form_container">
                                            <label for="chartCurrency">Measurement Unit:</label>
                                            <select name="chartCurrency" id="chartCurrency" class="chartOption1">
                                                <option value="Nigeria">USD</option>
                                            </select>
                                        </div>
                                        <div class="form_container">
                                            <label for="chartTime">Time period:</label>
                                            <select name="chartTime" id="chartTime" class="chartOption1">
                                                <option value="year">Year</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="chart_container">
                                        <canvas id="myChart_1" width="400" height="400"></canvas>

                                    </div>
                                </div>
                            </div>
                            <div class="chart_2">
                                <h4 class="chart_title">Track Record - <span>Amount Spent vs Date</span></h4>
                                <div class="underline"></div>
                                <div class="chart_">
                                    <div class="chart_select_options">
                                        <div class="form_container">
                                            <label for="chartCrop">Crop:</label>
                                            <select name="chartCrop" id="chartCrop" class="chartOption2">
                                            @foreach ($products as $product)
                                            <option value="{{$product->id}}" @if( old('product_id', $user->product_id)  == $product->id) selected="selected" @endif>{{$product->name}}</option>
                                            @endforeach
                                            </select>
                                        </div>

                                        <div class="form_container">
                                            <label for="chartCurrency">Currency Unit::</label>
                                            <select name="chartCurrency" id="chartCurrency" class="chartOption2">
                                                <option value="Nigeria">USD</option>
                                            </select>
                                        </div>
                                        <div class="form_container">
                                            <label for="chartTime">Time period:</label>
                                            <select name="chartTime" id="chartTime" class="chartOption2">
                                                <option value="year">Year</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="chart_container">
                                        <canvas id="myChart_2" width="400" height="400"></canvas>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="tab-pane fade show active" id="userProfile" role="tabpanel" aria-labelledby="userProfile-tab">
                        <form class="profile_container" id="profile_container" action="{{url('update_profile')}}" method="POST"  enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="profile_img">
                                @if (!empty($user->profile_image_url))
                                <div class=" edit_container" id="image-preview">
                                    <img src="{{asset(str_replace('public','storage',$user->profile_image_url))}}" alt="Profile image"
                                    class="img-fluid profile__img_ img picture">
                                    {{-- <div class="user_placeholder"></div> --}}
                                    <label class="btn btn-default label-btn" for="upload-file-selector">
                                        <svg width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.98413 23.2175L26.1144 1.30715C26.7969 0.630831 27.896 0.631035 28.5785 1.30715L32.9386 5.62412C33.621 6.29962 33.6211 7.38781 32.9386 8.06352L10.8083 29.9738L0.800781 33.2L3.98413 23.2175Z" stroke="white" stroke-width="1.4" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M29.6706 11.2489L22.9766 4.6203" stroke="white" stroke-width="0.8"/>
                                            <path d="M10.9449 29.0897L4.89062 23.0954" stroke="white" stroke-width="0.8"/>
                                            <path d="M9.4375 22.7423L23.2754 9.04175" stroke="white" stroke-width="0.8" stroke-linecap="round"/>
                                        </svg>
                                    </label>
                                    <div id="profilepicture" >
                                        <input id="upload-file-selector" name="profile_image_url" accept="image/jpeg,image/png" type="file" class="hidden">
                                    </div>
                                </div>
                                @else
                                <div class=" edit_container" id="image-preview">
                                    <img src="{{asset('images/profile1.png')}}" alt="Profile image" class="img-fluid profile__img_ hidden img picture">
                                    <div class="user_placeholder"></div>
                                    <label class="btn btn-default label-btn" for="upload-file-selector">
                                        <svg width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M3.98413 23.2175L26.1144 1.30715C26.7969 0.630831 27.896 0.631035 28.5785 1.30715L32.9386 5.62412C33.621 6.29962 33.6211 7.38781 32.9386 8.06352L10.8083 29.9738L0.800781 33.2L3.98413 23.2175Z" stroke="white" stroke-width="1.4" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M29.6706 11.2489L22.9766 4.6203" stroke="white" stroke-width="0.8"/>
                                            <path d="M10.9449 29.0897L4.89062 23.0954" stroke="white" stroke-width="0.8"/>
                                            <path d="M9.4375 22.7423L23.2754 9.04175" stroke="white" stroke-width="0.8" stroke-linecap="round"/>
                                        </svg>
                                    </label>
                                    <div id="profilepicture" >
                                        <input id="upload-file-selector" name="profile_image_url" accept="image/jpeg,image/png" type="file" class="hidden">
                                    </div>
                                </div>
                            @endif
                            <p class="name_">
                                <span id="firstname_Span"></span>
                                <span id="lastname_Span"></span>
                            </p>
                            <p class="country_">
                                <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="4" cy="4" r="4" fill="#263308"/>
                                </svg>
                                <span id="country_Span"></span>
                            </p>
                            </div>
                            <div>
                                <div class="input_container">
                                    <label for="firstname_">First Name</label>
                                    <input type="text" name="first_name" value="{{old('first_name', $user->first_name)}}" id="firstname_" class="textButton">
                                    @error('first_name')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="input_container">
                                    <label for="lastname_">Last Name</label>
                                    <input type="text" name="last_name" value="{{old('last_name', $user->last_name)}}" id="lastname_" class="textButton">
                                    @error('last_name')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="input_container">
                                    <label for="email_">Email</label>
                                    <input type="email" value="{{old('email', $user->email)}}" disabled="disabled" id="email_">
                                </div>
                                <div class="input_container">
                                    <label for="phone_">Phone Number</label>
                                    <input type="tel" value="{{old('phone', $user->phone)}}" disabled="disabled" id="phone_">
                                </div>
                                <div class="input_container">
                                    <label for="country_">Business Country Location</label>
                                    <select name="country_id" id="country_" class="textButton">
                                            @foreach ($countries as $country)
                                            <option value="{{$country->id}}" @if( old('country_id', $user->country_id)  == $country->id) selected="selected" @endif>{{$country->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="input_container">
                                    <label for="harvestType">Harvest Interested In</label>
                                    <select name="product_id" id="harvestType" class="">
                                        @foreach ($products as $product)
                                        <option value="{{$product->id}}" @if( old('product_id', $user->product_id)  == $product->id) selected="selected" @endif>{{$product->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input_container">
                                    <label for="oldPass">Old Password</label>
                                    <input type="password" name="old_password" id="oldPass" class="">
                                    @error('old_password')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="input_container">
                                    <label for="newPass">New Password</label>
                                    <input type="password" name="new_password" id="newPass" class="">
                                    @error('new_password')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="input_container">
                                    <label for="confirmNewPass">Confirm New Password</label>
                                    <input type="password" name="confirm_new_password" id="confirmNewPass" class="">
                                    @error('confirm_new_password')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="submit_btn">
                                    <button>Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </div>
    </main>
@endsection('content')
@section('scripts')
<script src="{{ asset('js/charts.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script>
// For profile page
let profileEdit = document.getElementById("profile_container");
if(profileEdit){
    for( const innerText of inputFields){
        // get the value of the field
        let inputValue = innerText.value;
        // create a new id from the field's id
        let inputId = `${innerText.id}Span`;
        // get the elemnt with the created id
        let outputSpan= document.getElementById(inputId);
        // output the result in the page
        outputSpan.innerHTML = inputValue;
    }
  }
</script>
@endsection('scripts')
