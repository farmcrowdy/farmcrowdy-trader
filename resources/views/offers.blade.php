@extends('layouts.fctrader')

@include('fctrader.nav')
@section('content')
    <main>
        @include('fctrader.page_header')
        <div class="main_body">
            <div class="header__">
                <h4 class="title">Offers</h4>
                    <div>
                        <a class="offers" href="{{ route('create_offer') }}"><svg width="17" height="18" viewBox="0 0 17 18" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M6.81863 7.19C7.31026 7.19 7.7088 6.78706 7.7088 6.29C7.7088 5.79294 7.31026 5.39 6.81863 5.39C6.32701 5.39 5.92847 5.79294 5.92847 6.29C5.92847 6.78706 6.32701 7.19 6.81863 7.19Z"
                                    fill="white" />
                                <path
                                    d="M10.1817 12.39C10.6733 12.39 11.0718 11.9871 11.0718 11.49C11.0718 10.9929 10.6733 10.59 10.1817 10.59C9.69005 10.59 9.2915 10.9929 9.2915 11.49C9.2915 11.9871 9.69005 12.39 10.1817 12.39Z"
                                    fill="white" />
                                <path
                                    d="M16.9071 11.07L15.8983 8.99L16.9071 6.91C17.1247 6.45 16.9467 5.91 16.5115 5.67L14.4938 4.59L14.0982 2.31C14.019 1.81 13.5443 1.47 13.0497 1.55L10.7749 1.87L9.13301 0.27C8.77694 -0.09 8.20328 -0.09 7.84721 0.27L6.20535 1.87L3.95026 1.53C3.45572 1.45 3.00075 1.79 2.90184 2.29L2.50621 4.57L0.488498 5.67C0.0533057 5.91 -0.124728 6.45 0.0928687 6.91L1.10172 8.99L0.0928687 11.07C-0.124728 11.53 0.0533057 12.07 0.488498 12.31L2.50621 13.39L2.90184 15.67C2.98097 16.17 3.45572 16.51 3.95026 16.43L6.22513 16.11L7.86699 17.73C8.22306 18.09 8.79672 18.09 9.15279 17.73L10.7947 16.11L13.0497 16.45C13.5443 16.53 13.9993 16.19 14.0982 15.69L14.4938 13.41L16.5115 12.33C16.9467 12.07 17.1247 11.53 16.9071 11.07ZM5.13715 6.29C5.13715 5.35 5.88884 4.59 6.81857 4.59C7.7483 4.59 8.5 5.35 8.5 6.29C8.5 7.23 7.7483 7.99 6.81857 7.99C5.88884 7.99 5.13715 7.23 5.13715 6.29ZM6.34382 12.43C6.26469 12.53 6.146 12.59 6.02731 12.59C5.94819 12.59 5.86906 12.57 5.78994 12.51C5.6119 12.37 5.59212 12.13 5.71081 11.95L10.8342 5.15C10.9727 4.97 11.2101 4.95 11.3881 5.07C11.5661 5.21 11.5859 5.45 11.4672 5.63L6.34382 12.43ZM10.1814 13.19C9.2517 13.19 8.5 12.43 8.5 11.49C8.5 10.55 9.2517 9.79 10.1814 9.79C11.1112 9.79 11.8629 10.55 11.8629 11.49C11.8629 12.43 11.1112 13.19 10.1814 13.19Z"
                                    fill="white" />
                            </svg>
                            Create an Offer</a>
                    </div>
            </div>
            @if (session('new_offer'))
                <div class="ml-4 alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> {{ session('new_offer') }}
                </div>
            @endif
            @if (session('update_offer'))
                <div class="ml-4 alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> {{ session('update_offer') }}
                </div>
            @endif
            @if (session('clone_offer'))
                <div class="ml-4 alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> {{ session('clone_offer') }}
                </div>
            @endif
            @if (session('delete_offer'))
                <div class="ml-4 alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> {{ session('delete_offer') }}
                </div>
            @endif
            <ul class="nav nav-offers" id="offers-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="offers-all-tab" data-toggle="pill" href="#offers-all" role="tab"
                        aria-controls="offers-all" aria-selected="true">All Offers</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="offers-ongoing-tab" data-toggle="pill" href="#offers-ongoing" role="tab"
                        aria-controls="offers-ongoing" aria-selected="false">Ongoing Offers</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="offers-completed-tab" data-toggle="pill" href="#offers-completed" role="tab"
                        aria-controls="offers-completed" aria-selected="false">Completed Offers</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="offers-failed-tab" data-toggle="pill" href="#offers-failed" role="tab"
                        aria-controls="offers-failed" aria-selected="false">Failed Offers</a>
                </li>
            </ul>
            <div class="tab-content" id="offers-tabContent">
                <div class="tab-pane fade show active" id="offers-all" role="tabpanel" aria-labelledby="offers-all-tab">
                    @foreach($offers as $offer)
                        @include('offer_card')
                    @endforeach
                </div>
            </div>
        </div>
    </main>
@endsection