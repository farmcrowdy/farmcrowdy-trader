@extends('layouts.fctrader')

@section('head_scripts')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="{{ asset('css/fcTrader.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/traderMsg.min.css') }}">
@endsection('head_scripts')

@section('nav')
@include('fctrader.nav')
@endsection('nav')
@section('content')
    <main>
    @include('fctrader.page_header')
        <div class="main_body">
            <div class="header__">
                <h4 class="title">Offer <span>#{{$offer->id}}</span></h4>

            </div>
            <section class="selected_offer_container">
                <div class="img_cont">
                @if (!empty($offer->file_path))
                <img src="{{asset(str_replace('public','storage',$offer->file_path))}}" alt="Offer image">
                @else
                <img src="{{asset('images/no-image.png')}}" alt="Order image">
                @endif
                </div>
                <div class="details_container">
                    <div class="name_header">
                        <h5>{{$offer->product->name}}</h5>
                        <input type="hidden" name="messageable_type" id="messageable_type" value="Offer">
                        <input type="hidden" name="messageable_id" id="messageable_id" value="{{$offer->id}}">
                        <!-- ongoing  -->
                        <p class="ongoing">
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M6.30694 13.0815C6.30895 11.9072 6.6158 10.7535 7.19744 9.73329C7.77909 8.71312 8.61561 7.86142 9.62516 7.26153C10.6347 6.66164 11.7827 6.33411 12.9568 6.31099C14.1309 6.28786 15.2909 6.56993 16.3233 7.1296C16.8472 7.41623 17.3284 7.77476 17.7529 8.19477V2.58861C17.7357 1.88807 17.4421 1.22276 16.9362 0.737866C16.4303 0.252977 15.7532 -0.0121213 15.0526 0.000425942H2.7003C1.99809 -0.0120934 1.31954 0.254283 0.813382 0.741179C0.307221 1.22807 0.0147264 1.89577 0 2.59795V14.2214C0.0147264 14.9236 0.307221 15.5913 0.813382 16.0782C1.31954 16.5651 1.99809 16.8314 2.7003 16.8189H7.47489C7.14736 16.3307 6.88949 15.7993 6.70871 15.2399C6.45928 14.5468 6.3236 13.8179 6.30694 13.0815ZM3.73744 4.67223C3.73744 4.42442 3.83589 4.18676 4.01111 4.01154C4.18634 3.83631 4.424 3.73787 4.6718 3.73787H13.0811C13.3289 3.73787 13.5665 3.83631 13.7417 4.01154C13.917 4.18676 14.0154 4.42442 14.0154 4.67223C14.0154 4.92004 13.917 5.1577 13.7417 5.33292C13.5665 5.50815 13.3289 5.60659 13.0811 5.60659H4.6718C4.424 5.60659 4.18634 5.50815 4.01111 5.33292C3.83589 5.1577 3.73744 4.92004 3.73744 4.67223Z"
                                    fill="#CCA91C" />
                                <path
                                    d="M13.5634 7.49009C10.2103 7.49009 7.49023 10.2102 7.49023 13.5633C7.49023 16.9164 10.2103 19.6333 13.5634 19.6333C16.9166 19.6333 19.6334 16.9164 19.6334 13.5633C19.6334 10.2102 16.9166 7.49009 13.5634 7.49009ZM16.363 14.0691C16.2453 14.1996 16.0512 14.26 15.7744 14.2568H11.3492C11.0756 14.2568 10.8783 14.1931 10.7607 14.0659C10.643 13.9418 10.5857 13.7732 10.5857 13.5664C10.5857 13.3596 10.6429 13.191 10.7607 13.0606C10.8783 12.9333 11.0756 12.8696 11.3492 12.8696H15.7744C16.0512 12.8696 16.2453 12.9333 16.363 13.0606C16.4806 13.191 16.5411 13.3596 16.5411 13.5664C16.5411 13.7732 16.4806 13.9419 16.363 14.0691Z"
                                    fill="#CCA91C" />
                                <rect x="9.81641" y="11.78" width="7.8533" height="3.92665" fill="#CCA91C" />
                            </svg> {{$offer->status}}
                        </p>
                    </div>
                    <div class="details_flex">
                        <ul class="details_">
                          {{-- @if ($offer->color)
                            <li>Color: <span>{{$offer->color}}</span></li>
                          @endif
                          @if ($offer->hardness)
                            <li>Hardness: <span>{{$offer->hardness}}</span></li>
                          @endif
                          @if ($offer->size)
                            <li>Size: <span>{{$offer->size}}</span></li>
                          @endif
                          @if ($offer->dryness_process)
                            <li>Drying Process: <span>{{$offer->dryness_process}}</span></li>
                          @endif
                          @if ($offer->test_weight)
                            <li>Test Weight: <span>{{$offer->test_weight}}%</span></li>
                          @endif
                          @if ($offer->curcumin_content)
                            <li>Curcumin Content: <span>{{$offer->curcumin_content}}%</span></li>
                          @endif
                          @if ($offer->mold_by_weight)
                            <li>Mold % by Weight: <span>{{$offer->mold_by_weight}}%</span></li>
                          @endif
                          @if ($offer->extraneous_by_weight)
                            <li>Extraneous % by Weight: <span>{{$offer->extraneous_by_weight}}%</span></li>
                          @endif
                          @if ($offer->whole_dead_insects)
                            <li>Whole Dead Insects: <span>{{$offer->whole_dead_insects}}%</span></li>
                          @endif --}}
                            <li>Color: <span>{{$offer->color ?? 'Not specified'}}</span></li>
                            <li>Hardness: <span>{{$offer->hardness ?? 'Not specified'}}</span></li>
                            <li>Size: <span>{{$offer->size ?? 'Not specified'}}</span></li>
                            <li>Drying Process: <span>{{$offer->dryness_process ?? 'Not specified'}}</span></li>
                            <li>Test Weight: <span>{{$offer->test_weight ?? 'Not specified'}}%</span></li>
                            <li>Curcumin Content: <span>{{$offer->curcumin_content ?? 'Not specified'}}%</span></li>
                            <li>Mold % by Weight: <span>{{$offer->mold_by_weight ?? 'Not specified'}}%</span></li>
                            <li>Extraneous % by Weight: <span>{{$offer->extraneous_by_weight ?? 'Not specified'}}%</span></li>
                            <li>Whole Dead Insects: <span>{{$offer->whole_dead_insects ?? 'Not specified'}}%</span></li>
                        </ul>
                        <ul class="details_">
                            {{-- @if ($offer->moisture)
                              <li>Moisture: <span> {{$offer->moisture}}%</span></li>
                            @endif --}}
                            <li>Moisture: <span> {{$offer->moisture ?? '0'}}%</span></li>
                            <li>Oil content: <span> {{$offer->oil_content ?? '0'}}%</span></li>
                            <li>Foreign Matter: <span> {{$offer->foreign_matter ?? '0'}}%</span></li>
                            <li>Broken Material: <span>{{$offer->broken_material ?? '0'}}%</span></li>
                            <li>Damaged Kernel: <span>{{$offer->damaged_kernel ?? '0'}}%</span></li>
                            <li>Rotten/Shrivelled: <span>{{$offer->rotten ?? '0'}}%</span></li>
                            <li>Ash Content: <span>{{$offer->ash_content ?? '0'}}%</span></li>
                            <li>Dockage: <span>{{$offer->dockage ?? '0'}}%</span></li>
                            <li>Insect Defiled %: <span> {{$offer->insect_defiled ?? '0'}}%</span></li>
                        </ul>
                        <ul class="details_">
                            <li>Splits: <span>{{$offer->splits ?? '0'}}%</span></li>
                            <li>Weevil: <span>{{$offer->Weevil ?? '0'}}%</span></li>
                            <li>Volatile: <span>{{$offer->volatile ?? '0'}}%</span></li>
                            <li>Mammalican: <span>{{$offer->mammalican ?? '0'}}%</span></li>
                            <li>Infestations: <span>{{$offer->infestations ?? '0'}}%</span></li>
                            <li>Total Defects: <span>{{$offer->total_defects ?? '0'}}%</span></li>
                            <li>Hectoliters Weight: <span>{{$offer->hectoliter_weight ?? '0'}}%</span></li>
                            <li>Acid Insoluble Ash: <span> {{$offer->acid_insoluble_ash ?? '0'}}%</span></li>
                        </ul>
                    </div>
                    @if(Auth::user()->id == $offer->user->id)
                    <a href="{{ url("edit_offer/$offer->id") }}" onclick="confirmAction('edit')" class="btn btn-success respond">Edit</a>
                    <a href="{{ url("clone_offer/$offer->id") }}" onclick="confirmAction('clone')" class="btn btn-warning respond">Clone</a>
                    <a href="{{ url("delete_offer/$offer->id") }}" onclick="confirmAction('delete')" class="btn btn-danger respond">Delete</a>
                    <script type="text/javascript">
                        function confirmAction(action){
                            window.event.preventDefault();
                            let answer = confirm("Do you want to " + action + " this Offer?");
                            if (answer) {
                                window.location = window.event.target.getAttribute('href');
                            }
                        }
                    </script>
                    @endif
                </div>
            </section>
            <section>
                <div class="section_nav">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        {{-- <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="inquiry-tab" data-toggle="tab" href="#inquiry" role="tab"
                                aria-controls="inquiry" aria-selected="true">Inquiries</a>
                        </li> --}}
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="message-tab" data-toggle="tab" href="#message" role="tab"
                                aria-controls="message" aria-selected="true">Messages</a>
                        </li>
                        {{-- <li class="nav-item" role="presentation">
                            <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab"
                                aria-controls="payment" aria-selected="false">Payments</a>
                        </li> --}}
                    </ul>
                    {{-- <div class="sort">Sort by: <select>
                            <option value="">Date</option>
                            <option value="">Price</option>
                            <option value="">Quantity</option>
                        </select>
                    </div> --}}

                </div>
                <div class="tab-content" id="myTabContent">
                    {{-- <div class="tab-pane fade show active" id="inquiry" role="tabpanel" aria-labelledby="inquiry-tab">
                        <!-- enquiry item  -->
                        <div class="accordion" id="inquiry_1">
                            <div class="inquiry_card">
                                <div class="inquiry_header" id="inquiry_One">
                                    <div class="name">
                                        <p class="p_name">Buyer name: <span>Farmcrowdy Ltd</span></p>
                                        <p>Nigeria <svg width="6" height="6" viewBox="0 0 6 6" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="2.78279" cy="3.01958" r="2.56795" fill="#C4C4C4" />
                                            </svg>
                                            12:30pm, today
                                        </p>
                                    </div>
                                    <div class="msg">
                                        <h6>Message</h6>
                                        <p>We have the item available, but its not exactly
                                            according to your request.</p>
                                    </div>
                                    <div class="btns">
                                        <a href="#" class="respond">Yet to Respond</a>
                                        <a href="#" class="msg_link">
                                            <svg width="19" height="18" viewBox="0 0 19 18" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.53799 10.7723L15.2319 1.23746C15.5309 0.943141 16.0123 0.94323 16.3113 1.23746L18.2212 3.11611C18.5201 3.41007 18.5201 3.88363 18.2212 4.17768L8.52724 13.7126L4.14355 15.1165L5.53799 10.7723Z"
                                                    stroke="#86BA16" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M16.7878 5.56388L13.8555 2.67926" stroke="#86BA16"
                                                    stroke-width="0.6" />
                                                <path d="M8.14517 13.3277L5.49316 10.7191" stroke="#86BA16"
                                                    stroke-width="0.5" />
                                                <path d="M7.92383 10.5654L13.9854 4.60324" stroke="#86BA16"
                                                    stroke-width="0.6" stroke-linecap="round" />
                                                <path
                                                    d="M15.9516 9.87306L15.9517 16.6338C15.9517 17.0869 15.5807 17.4515 15.1201 17.4516L1.81495 17.4515C1.35429 17.4516 0.983415 17.0869 0.983414 16.6338L0.983398 3.54685C0.983365 3.09352 1.35428 2.7287 1.81495 2.7287L10.2293 2.72881"
                                                    stroke="#86BA16" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            Message Seller</a>

                                    </div>
                                    <div class="btn_">
                                        <button class="icon" type="button" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne">

                                        </button>
                                    </div>
                                </div>

                                <div id="collapseOne" class="collapse body_ show" aria-labelledby="inquiry_One"
                                    data-parent="#inquiry_1">
                                    <div class="card-body">
                                        <p>Sellers specifition according to your request</p>
                                        <div class="details_flex">
                                            <ul class="details_">
                                                <li>Color: <span>White</span></li>
                                                <li>Hardness: <span>Soft</span></li>
                                                <li>Size: <span>Whole Grain</span></li>
                                                <li>Drying Process: <span>Air Dried</span></li>
                                                <li>Test Weight: <span>Bushel</span></li>
                                                <li>Curcumin Content: <span>0.5%</span></li>
                                                <li>Mold % by Weight: <span>0.5%</span></li>
                                                <li>Extraneous % by Weight: <span>0.5%</span></li>
                                                <li>Whole Dead Insects: <span>0.5%</span></li>
                                            </ul>
                                            <ul class="details_">
                                                <li>Moisture: <span> 2%</span></li>
                                                <li>Oil content: <span> 0.5%</span></li>
                                                <li>Foreign Matter: <span> 0%</span></li>
                                                <li>Broken Material: <span>0.0%</span></li>
                                                <li>Damaged Kernel: <span>1.0%</span></li>
                                                <li>Rotten/Shrivelled: <span>1.0%</span></li>
                                                <li>Ash Content: <span>1.8%</span></li>
                                                <li>Dockage: <span>1.0%</span></li>
                                                <li>Insect Defiled %: <span> 0.5%</span></li>
                                            </ul>
                                            <ul class="details_">
                                                <li>Splits: <span>18.0%</span></li>
                                                <li>Weevil: <span>0.0%</span></li>
                                                <li>Volatile: <span>2.0%</span></li>
                                                <li>Mammalian: <span>0.5%</span></li>
                                                <li>Infestations: <span>0.0%</span></li>
                                                <li>Total Defects: <span>1.0%</span></li>
                                                <li>Hectolites Weight: <span>4.0%</span></li>
                                                <li>Acid Insoluble Ash: <span> 2.0%</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="inquiry_footer">
                                    <p class="price">$300</p>
                                    <div class="btns">
                                        <p>Like this supplier’s sample, proceed
                                            to make payment.</p>
                                        <a href="#">Decline Offer</a>
                                        <a href="#" class="make_payment">Make Payment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of item -->
                        <div class="accordion" id="inquiry_2">
                            <div class="inquiry_card">
                                <div class="inquiry_header" id="inquiry_Two">
                                    <div class="name">
                                        <p class="p_name">Buyer name: <span>Farmcrowdy Ltd</span></p>
                                        <p>Nigeria <svg width="6" height="6" viewBox="0 0 6 6" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="2.78279" cy="3.01958" r="2.56795" fill="#C4C4C4" />
                                            </svg>
                                            12:30pm, today
                                        </p>
                                    </div>
                                    <div class="msg">
                                        <h6>Message</h6>
                                        <p>We have the item available, but its not exactly
                                            according to your request.</p>
                                    </div>
                                    <div class="btns">
                                        <a href="#" class="respond">Yet to Respond</a>
                                        <a href="#" class="msg_link">
                                            <svg width="19" height="18" viewBox="0 0 19 18" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.53799 10.7723L15.2319 1.23746C15.5309 0.943141 16.0123 0.94323 16.3113 1.23746L18.2212 3.11611C18.5201 3.41007 18.5201 3.88363 18.2212 4.17768L8.52724 13.7126L4.14355 15.1165L5.53799 10.7723Z"
                                                    stroke="#86BA16" stroke-linecap="round" stroke-linejoin="round" />
                                                <path d="M16.7878 5.56388L13.8555 2.67926" stroke="#86BA16"
                                                    stroke-width="0.6" />
                                                <path d="M8.14517 13.3277L5.49316 10.7191" stroke="#86BA16"
                                                    stroke-width="0.5" />
                                                <path d="M7.92383 10.5654L13.9854 4.60324" stroke="#86BA16"
                                                    stroke-width="0.6" stroke-linecap="round" />
                                                <path
                                                    d="M15.9516 9.87306L15.9517 16.6338C15.9517 17.0869 15.5807 17.4515 15.1201 17.4516L1.81495 17.4515C1.35429 17.4516 0.983415 17.0869 0.983414 16.6338L0.983398 3.54685C0.983365 3.09352 1.35428 2.7287 1.81495 2.7287L10.2293 2.72881"
                                                    stroke="#86BA16" stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            Message Seller</a>

                                    </div>
                                    <div class="btn_">
                                        <button class="icon" type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="true" aria-controls="collapseTwo">

                                        </button>
                                    </div>
                                </div>

                                <div id="collapseTwo" class="collapse body_" aria-labelledby="inquiry_Two"
                                    data-parent="#inquiry_2">
                                    <div class="card-body">
                                        <p>Sellers specifition according to your request</p>
                                        <div class="details_flex">
                                            <ul class="details_">
                                                <li>Color: <span>White</span></li>
                                                <li>Hardness: <span>Soft</span></li>
                                                <li>Size: <span>Whole Grain</span></li>
                                                <li>Drying Process: <span>Air Dried</span></li>
                                                <li>Test Weight: <span>Bushel</span></li>
                                                <li>Curcumin Content: <span>0.5%</span></li>
                                                <li>Mold % by Weight: <span>0.5%</span></li>
                                                <li>Extraneous % by Weight: <span>0.5%</span></li>
                                                <li>Whole Dead Insects: <span>0.5%</span></li>
                                            </ul>
                                            <ul class="details_">
                                                <li>Moisture: <span> 2%</span></li>
                                                <li>Oil content: <span> 0.5%</span></li>
                                                <li>Foreign Matter: <span> 0%</span></li>
                                                <li>Broken Material: <span>0.0%</span></li>
                                                <li>Damaged Kernel: <span>1.0%</span></li>
                                                <li>Rotten/Shrivelled: <span>1.0%</span></li>
                                                <li>Ash Content: <span>1.8%</span></li>
                                                <li>Dockage: <span>1.0%</span></li>
                                                <li>Insect Defiled %: <span> 0.5%</span></li>
                                            </ul>
                                            <ul class="details_">
                                                <li>Splits: <span>18.0%</span></li>
                                                <li>Weevil: <span>0.0%</span></li>
                                                <li>Volatile: <span>2.0%</span></li>
                                                <li>Mammalian: <span>0.5%</span></li>
                                                <li>Infestations: <span>0.0%</span></li>
                                                <li>Total Defects: <span>1.0%</span></li>
                                                <li>Hectolites Weight: <span>4.0%</span></li>
                                                <li>Acid Insoluble Ash: <span> 2.0%</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="inquiry_footer">
                                    <p class="price">$300</p>
                                    <div class="btns">
                                        <p>Like this supplier’s sample, proceed
                                            to make payment.</p>
                                        <a href="#">Decline Offer</a>
                                        <a href="#" class="make_payment">Make Payment</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="tab-pane fade show active" id="message" role="tabpanel" aria-labelledby="message-tab">
                        <section class="msg_body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12 px-0">
                                        <div class="inbox">
                                            <div class="header">
                                                <h5 class="title">Messages</h5>
                                            <p class="sub_title">Communicate with Administrator here.</p>
                                            </div>
                                            <div class="underline"></div>
                                            <input type="checkbox" name="" id="chatsMenu">
                                            <label for="chatsMenu" class="chatsMenu">
                                                <span>
                                                    <svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M9.88872 0.00195312H2.12498C1.40932 0.00195312 0.829193 0.582124 0.829193 1.29774V7.74109C0.829193 8.45111 1.40032 9.02744 2.10818 9.03644V9.03684L2.46922 9.24116H4.52926L5.01259 9.03688H9.8886C10.6043 9.03688 11.1844 8.45675 11.1844 7.74109V1.29774C11.1844 0.582164 10.6043 0.00203313 9.88872 0.00195312V0.00195312Z" fill="#6B61B1"/>
                                                        <path d="M12.3718 0.00170898H9.77356C10.4892 0.00170898 11.0693 0.581839 11.0693 1.29749V7.74085C11.0693 8.4565 10.4892 9.03663 9.77356 9.03663H12.3718C13.0875 9.03663 13.6676 8.4565 13.6676 7.74085V1.29749C13.6676 0.58188 13.0875 0.00170898 12.3718 0.00170898V0.00170898Z" fill="#5E54AC"/>
                                                        <path d="M2.10815 12.0466C2.10815 12.2354 2.34644 12.3182 2.46348 12.17L4.78284 9.23375C4.88117 9.10926 5.03109 9.03662 5.18973 9.03662H2.10815V12.0466Z" fill="#5E54AC"/>
                                                        <path d="M16.7679 14.5397L17.2333 15.129C17.3132 15.126 17.378 15.0653 17.3885 14.986C17.4352 14.632 17.6609 14.3274 17.9912 14.172C18.4967 13.9342 18.8466 13.4204 18.8466 12.8247V5.42308C18.8466 4.60102 18.1802 3.93457 17.3581 3.93457H8.05009C7.228 3.93457 6.56158 4.60098 6.56158 5.42308V12.8247C6.56158 13.6468 7.228 14.3132 8.05009 14.3132H16.3004C16.4827 14.3132 16.6549 14.3967 16.7679 14.5397V14.5397Z" fill="#E49542"/>
                                                        <path d="M19.8211 3.93457H17.2141C18.0361 3.93457 18.7026 4.60098 18.7026 5.42308V12.8247C18.7026 13.4204 18.3526 13.9342 17.8471 14.172C17.4747 14.3472 17.2334 14.7174 17.2334 15.1289L19.4322 17.9127C19.5667 18.0829 19.8404 17.9878 19.8404 17.7709V14.3127C20.6535 14.3024 21.3096 13.6403 21.3096 12.8247V5.42308C21.3096 4.60098 20.6432 3.93457 19.8211 3.93457V3.93457Z" fill="#E27D47"/>
                                                        <path d="M18.8394 6.94646H9.03185C8.86117 6.94646 8.72281 6.8081 8.72281 6.63741C8.72281 6.46673 8.86117 6.32837 9.03185 6.32837H18.8394C19.01 6.32837 19.1484 6.46673 19.1484 6.63741C19.1484 6.8081 19.01 6.94646 18.8394 6.94646Z" fill="#F0F7FF"/>
                                                        <path d="M18.8393 9.30901H9.03182C8.86114 9.30901 8.72278 9.17065 8.72278 8.99996C8.72278 8.82928 8.86114 8.69092 9.03182 8.69092H18.8393C19.01 8.69092 19.1484 8.82928 19.1484 8.99996C19.1484 9.17061 19.01 9.30901 18.8393 9.30901Z" fill="#F0F7FF"/>
                                                        <path d="M18.8393 11.6716H9.03182C8.86114 11.6716 8.72278 11.5332 8.72278 11.3625C8.72278 11.1918 8.86114 11.0535 9.03182 11.0535H18.8393C19.01 11.0535 19.1484 11.1918 19.1484 11.3625C19.1484 11.5332 19.01 11.6716 18.8393 11.6716Z" fill="#F0F7FF"/>
                                                    </svg>
                                                </span>
                                                Chats</label>
                                        <div class="flex_container">
                                                <div class="messenger-list">
                                                    <div class="search-bar">
                                                        <input placeholder="Search here or start new chat" id="mySearch">
                                                    </div>
                                                    <div class="row">
                                                    </div>
                                                </div>
                                                <div class="chat-box">
                                                    <div class="chat-header">
                                                    </div>
                                                    <div class="chatter" id="chatArea" style="padding-bottom: 160px;">
                                                    </div>
                                                    <div class="chat-footer">
                                                        <div class="chat-input">
                                                            <textarea id="comment" rows="1" placeholder="Type your message here..."></textarea>
                                                            <ul class="chat-action">
                                                                <li>
                                                                    <span id="fileContainer" ></span>
                                                                    <label for="uploadDoc">
                                                                        <a><img src="{{asset('images/icons/attachment.svg')}}"/></a>
                                                                    </label>
                                                                    <span class="btn btn-secondary badge" id="removeFile">x</span>

                                                                    <input type="file" name="file_path" id="uploadDoc">
                                                                    <input id="user_id" type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                                                </li>
                                                            </ul>
                                                            <button id="send_message" class="btn btn-blue btn-sm chat-btn"><img src="{{asset('images/icons/fly-send.svg')}}"/></button>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    {{-- <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                        <div class="payment_table" id="paymentTable">
                            <div class="header">
                                <h5>Payment</h5>
                                <p>Track your activity in relation to this offer.</p>
                            </div>
                            <div class="underline"></div>
                            <div class="table_container">
                                <table id="PaymentDataTable" class="table dt-responsive" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Initiated ON</th>
                                            <th>Status</th>
                                            <th>Amount</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p class="payer_name">Ibukun Azeez</p>
                                                <p class="payer_id">PayID-001</p>
                                            </td>
                                            <td>
                                                <p>Mar 30, 2020</p>
                                                <span>6 Months ago</span>
                                            </td>
                                            <!-- change the class from "success" to "failed" the transaction wasn't successful  -->
                                            <td class="sucess status">
                                                <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <ellipse cx="5" cy="5" rx="5" ry="5" fill="#19A784" />
                                                </svg>
                                                <span>Successful</span>
                                            </td>
                                            <td class="amount">5243.99</td>
                                            <td>
                                                <a href="javascript:void(0)">
                                                    <img src="{{asset('images/icons/download.svg')}}" alt="Download icon">
                                                    Invoice
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="payer_name">Boyd Andersen</p>
                                                <p class="payer_id">PayID-001</p>
                                            </td>
                                            <td>
                                                <p>Mar 30, 2020</p>
                                                <span>6 Months ago</span>
                                            </td>
                                            <td class="failed status">
                                                <svg width="10" height="10" viewBox="0 0 10 10" fill="none"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <ellipse cx="5" cy="5" rx="5" ry="5" fill="#19A784" />
                                                </svg>
                                                <span>Failed</span>
                                            </td>
                                            <td class="amount">5243.99</td>
                                            <td>
                                                <a href="#">
                                                    <img src="{{asset('images/icons/download.svg')}}" alt="Download icon">
                                                    Invoice
                                                </a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </section>
        </div>
    </main>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/message.js') }}"></script>
@endsection('scripts')
