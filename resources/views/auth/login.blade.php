@extends('layouts.trader_auth')
@section('content')
<section>
    <div class="auth_container">
        <div class="auth_img">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/logo/desktop.svg') }}" alt="FC Trader logo">
                </a>
            </div>
            <img
                sizes="(max-width: 640px) 100vw, 640px"
                srcset="
                images/bg/auth/auth_ioqt2y_c_scale,w_310.jpg 310w,
                images/bg/auth/auth_ioqt2y_c_scale,w_366.jpg 366w,
                images/bg/auth/auth_ioqt2y_c_scale,w_413.jpg 413w,
                images/bg/auth/auth_ioqt2y_c_scale,w_468.jpg 468w,
                images/bg/auth/auth_ioqt2y_c_scale,w_513.jpg 513w,
                images/bg/auth/auth_ioqt2y_c_scale,w_554.jpg 554w,
                images/bg/auth/auth_ioqt2y_c_scale,w_604.jpg 604w,
                images/bg/auth/auth_ioqt2y_c_scale,w_625.jpg 625w,
                images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg 640w"
                src="images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg"
            alt="Auth hero">
        </div>
        <div class="form_container">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('images/logo/desktop.svg')}}" alt="FC Trader logo">
                </a>
            </div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h3>Log In to your Farmcrowdy
                    Trader Account</h3>
                <div class="alt_link">
                    <p>Don’t have an account? <a href="{{ route('register') }}">Sign Up</a></p>
                    <a href="{{ route('google') }}" class="google_button">
                        <svg class="mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.9573 9.09314C19.9167 8.66294 19.5556 8.33326 19.1234 8.33326H10.8334C10.3732 8.33326 10.0001 8.70636 10.0001 9.16661V10.8333C10.0001 11.2935 10.3732 11.6666 10.8334 11.6666H16.4556C15.7168 14.5388 13.1056 16.6666 10.0001 16.6666C6.07518 16.6666 2.9349 13.2627 3.37458 9.25002C3.67995 6.46301 6.33838 3.74963 9.11889 3.39031C11.0268 3.14373 12.805 3.7122 14.1545 4.79082C14.4874 5.05691 14.9635 5.03644 15.2649 4.73507L16.4484 3.55153C16.7928 3.20718 16.7744 2.63662 16.4007 2.32441C14.6648 0.874081 12.4319 0 10 0C4.1113 0 -0.61038 5.12509 0.0643452 11.1462C0.58429 15.7857 4.37674 19.5099 9.02415 19.9534C14.3934 20.4656 19.0153 16.6965 19.8555 11.6667C19.95 11.1222 20 10.5667 20 10C20 9.69445 19.9855 9.39217 19.9573 9.09314Z" fill="white"/>
                        </svg>
                           Login with Google</a>
                </div>
                <div class="input_group">
                    <div class="form_cont">
                        <label for="email">Business Email</label>
                        <input type="email" placeholder="Email" id="email" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                        @error('email')
                            <span class="error" style="color: #f00;" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form_cont">
                        <label for="password">Password</label>
                        <input type="password" placeholder="Password" id="password"
                         @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="error" style="color: #f00;" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="submit_">
                    <button>Log in</button>
                </div>

                {{-- <div class="submit_">
                    <a href="{{ route('google') }}">Login with Google</a>
                </div> --}}
                <div class="underline"></div>
                <div class="forgot_pass">
                    <a href="{{ route('password.request') }}">Forgot your password?</a>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
