@extends('layouts.trader_auth')
@section('content')
<section>
    <div class="auth_container">
        <div class="auth_img">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/logo/desktop.svg') }}" alt="FC Trader logo">
                </a>
            </div>
            <img
                sizes="(max-width: 640px) 100vw, 640px"
                srcset="
                /images/bg/auth/auth_ioqt2y_c_scale,w_310.jpg 310w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_366.jpg 366w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_413.jpg 413w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_468.jpg 468w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_513.jpg 513w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_554.jpg 554w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_604.jpg 604w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_625.jpg 625w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg 640w"
                src="/images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg"
            alt="Auth hero">
        </div>
        <div class="form_container">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('images/logo/desktop.svg')}}" alt="FC Trader logo">
                </a>
            </div>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <h3>Create your New Password</h3>
                  <div class="alt_link">
                    <p>Proceed by filling in the details below.</p>
                  </div>
                <input type="hidden" name="token" value="{{ $token }}">

                <input type="hidden" placeholder="Email" id="email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus />

                <div class="input_group">
                    <div class="form_cont">
                        <label for="password">Password</label>
                        <input id="password" type="password" placeholder="Password" name="password" required autocomplete="new-password">
                        @error('password')
                            <span class="error" style="color: #f00;" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form_cont">
                        <label for="confirmPass">Confirm Password</label>
                        <input type="password" placeholder="Confirm Password" id="confirmPass" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <div class="submit_">
                    <button type="submit">{{ __('Reset Password') }}</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
