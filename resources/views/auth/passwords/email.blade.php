@extends('layouts.trader_auth')
@section('content')
<section>
    <div class="auth_container">
        <div class="auth_img">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/logo/desktop.svg') }}" alt="FC Trader logo">
                </a>
            </div>
            <img
                sizes="(max-width: 640px) 100vw, 640px"
                srcset="
                /images/bg/auth/auth_ioqt2y_c_scale,w_310.jpg 310w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_366.jpg 366w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_413.jpg 413w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_468.jpg 468w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_513.jpg 513w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_554.jpg 554w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_604.jpg 604w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_625.jpg 625w,
                /images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg 640w"
                src="/images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg"
            alt="Auth hero">
        </div>
        <div class="form_container">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('images/logo/desktop.svg')}}" alt="FC Trader logo">
                </a>
            </div>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <h3>Reset Password</h3>
                <div class="alt_link">
                    <p>Enter your email and a reset link will be sent to your email.</p>
                </div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="input_group">
                    <div class="form_cont">
                        <label for="email">Email address</label>
                        <input type="email" placeholder="Johnnydoe@gmail.com" id="email" name="email" value="{{ old('email') }}" />
                        @error('email')
                            <span class="error" style="color: #f00;" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form_cont">

                    </div>
                </div>

                <div class="submit_">
                    <button type="submit">{{ __('Send Password Reset Link') }}</button>
                </div>
                <div class="alt_link pt-2">
                    <p>Have an account? <a href="{{ route('login') }}">Login</a></p>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
