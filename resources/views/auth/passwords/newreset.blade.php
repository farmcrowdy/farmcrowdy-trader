@extends('layouts.trader_auth')
@section('content')
<section>
    <div class="auth_container">
        <div class="auth_img">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/logo/desktop.svg') }}" alt="FC Trader logo">
                </a>
            </div>
            <img
                sizes="(max-width: 640px) 100vw, 640px"
                srcset="
                images/bg/auth/auth_ioqt2y_c_scale,w_310.jpg 310w,
                images/bg/auth/auth_ioqt2y_c_scale,w_366.jpg 366w,
                images/bg/auth/auth_ioqt2y_c_scale,w_413.jpg 413w,
                images/bg/auth/auth_ioqt2y_c_scale,w_468.jpg 468w,
                images/bg/auth/auth_ioqt2y_c_scale,w_513.jpg 513w,
                images/bg/auth/auth_ioqt2y_c_scale,w_554.jpg 554w,
                images/bg/auth/auth_ioqt2y_c_scale,w_604.jpg 604w,
                images/bg/auth/auth_ioqt2y_c_scale,w_625.jpg 625w,
                images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg 640w"
                src="images/bg/auth/auth_ioqt2y_c_scale,w_640.jpg"
            alt="Auth hero">
        </div>
        <div class="form_container">
            <div class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('images/logo/desktop.svg')}}" alt="FC Trader logo">
                </a>
            </div>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h3>Log In to your Farmcrowdy
                    Trader Account</h3>
                <div class="alt_link">
                    <p>Don’t  have an account? <a href="{{ route('register') }}">Sign Up</a></p>
                </div>
                <div class="input_group">
                    <div class="form_cont">
                        <label for="email">Business Email</label>
                        <input type="email" placeholder="Email" id="email" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                        @error('email')
                            <span class="error" style="color: #f00;" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form_cont">
                        <label for="password">Password</label>
                        <input type="password" placeholder="Password" id="password"
                         @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="error" style="color: #f00;" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="submit_">
                    <button>Log in</button>
                </div>
                <div class="underline"></div>
                <div class="forgot_pass">
                    <a href="{{ route('password.request') }}">Forgot your password?</a>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
