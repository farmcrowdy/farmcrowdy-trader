@extends('layouts.fctrader')

@section('nav')
@include('fctrader.nav')
@endsection('nav')
@section('content')

    <main>
        @include('fctrader.page_header')
        <div class="main_body">
            <div class="header__">
                <h4 class="title">Quality Specification</h4>
                @if($errors->any())
                    <div class="ml-4 alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Please check the fields in red for correction.</strong>
                    </div>
                @endif
            </div>

            <div class="create_offer_form">
                <div class="form_step_container">
                    <div class="wrapper">
                        <ul class="steps">
                            <li class="is-active">
                                <div class="position">
                                    <span>1</span>
                                </div>
                                <p>Quality &amp; Quantity
                                    Specification</p>
                            </li>
                            <li>
                                <div class="position">
                                    <span>2</span>
                                </div>
                                <p>Delivery Location &amp; Terms
                                    of Payment</p>
                            </li>
                            <li>
                                <div class="position">
                                    <span>3</span>
                                </div>
                                <p>Review Specifications filled
                                    in Part One &amp; Two</p>
                            </li>
                        </ul>
                            <div id="error_container"></div>
                        <form class="form-wrapper" action="{{ route('new_order') }}" method="POST" enctype="multipart/form-data">
                        <div>
                          @csrf
                          <input type="hidden" value="{{Auth::user()->id}}" />
                        </div>
                            <fieldset class="section is-active">
                                <h4>What do you want to do?</h4>
                                <div class="underline"></div>
                                <div class="input_group userType">
                                    <div class="input_container ">
                                      <a href="{{route('create_order')}}">
                                        <input type="button" checked name="userType" id="buyerType">
                                        <label for="buyerType">I want to buy harvest</label>
                                      </a>
                                    </div>
                                    <div class="input_container">
                                      <a href="{{route('create_offer')}}">
                                        <input type="button" name="userType" id="sellerType">
                                        <label for="sellerType">I want to sell harvest</label>
                                      </a>
                                    </div>
                                </div>
                                <h4>Crop Category</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="input_group">
                                  <div class="input_container">
                                      <label for="product_id">Produce <span class="red">*</span></label>
                                      <select name="product_id" id="productID" class="textButton">
                                        <option value="">Select Produce</option>
                                          @foreach ($products as $product)
                                          <option value="{{$product->id}}" @if( old('product_id')  == $product->id) selected="selected" @endif>{{$product->name}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class="input_container">
                                      <label for="color">Color <span class="red">*</span></label>
                                      <input name="color" type="text" id="color" class="textButton" placeholder="White" value="{{old('color')}}">
                                      @error('color')
                                          <span style="color: #F00;">{{$message}}</span>
                                      @enderror
                                  </div>
                                </div>
                                <div class="input_group">
                                  <div class="input_container">
                                      <label for="size">Size <span class="red">*</span></label>
                                      <input name="size" type="text" id="size" class="textButton" placeholder="Whole Grain" value="{{old('size')}}">
                                      @error('size')
                                          <span style="color: #F00;">{{$message}}</span>
                                      @enderror
                                  </div>
                                  <div class="input_container">
                                      <label for="hardness">Hardness <span class="red">*</span></label>
                                      <input name="hardness" type="text" id="hardness" class="textButton" placeholder="Soft" value="{{old('hardness')}}">
                                      @error('hardness')
                                          <span style="color: #F00;">{{$message}}</span>
                                      @enderror
                                  </div>
                                </div>
                                <div class="input_group">
                                  <div class="input_container">
                                      <label for="Dryness">Dryness Process <span class="red">*</span></label>
                                      <input name="dryness_process" type="text" id="Dryness" class="textButton" placeholder="Air dried" value="{{old('dryness_process')}}">
                                      @error('dryness_process')
                                          <span style="color: #F00;">{{$message}}</span>
                                      @enderror
                                  </div>
                                    <div class="input_container">
                                    </div>
                                </div>
                                <h4 class="form_header">Quality Specifications</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Matter">Foreign Matter</label>
                                        <input name="foreign_matter" type="text" id="Matter" class="textButton" placeholder="0%" value="{{old('foreign_matter')}}">
                                        @error('foreign_matter')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Moisture">Moisture</label>
                                        <input name="moisture" type="text" id="Moisture" class="textButton" placeholder="0%" value="{{old('moisture')}}">
                                        @error('moisture')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Weevil">Weevil</label>
                                        <input name="weevil_process" type="text" id="Weevil" class="textButton" placeholder="0%" value="{{old('weevil_process')}}">
                                        @error('weevil_process')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="brokenMaterial">Broken Material</label>
                                        <input name="broken_material" type="text" id="brokenMaterial" class="textButton" placeholder="0%" value="{{old('broken_material')}}">
                                        @error('broken_material')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Rotten">Rotten/Shrivelled</label>
                                        <input name="rotten" type="text" id="Rotten" class="textButton" placeholder="0%" value="{{old('rotten')}}">
                                        @error('rotten')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="damagedKernel">Damaged Kernel</label>
                                        <input name="damaged_kernel" type="text" id="damagedKernel" class="textButton" placeholder="0%" value="{{old('damaged_kernel')}}">
                                        @error('damaged_kernel')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Splits">Splits</label>
                                        <input name="splits" type="text" id="Splits" class="textButton" placeholder="0%" value="{{old('splits')}}">
                                        @error('splits')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Weight">Test Weight</label>
                                        <input name="test_weight" type="text" id="Weight" class="textButton" placeholder="0%" value="{{old('test_weight')}}">
                                        @error('test_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Defects">Total Defects</label>
                                        <input name="total_defects" type="text" id="Defects" class="textButton" placeholder="0%" value="{{old('total_defects')}}">
                                        @error('total_defects')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="oilContent">Oil Content</label>
                                        <input name="oil_content" type="text" id="oilContent" class="textButton" placeholder="0%" value="{{old('oil_content')}}">
                                        @error('oil_content')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Dockage">Dockage</label>
                                        <input name="dockage" type="text" id="Dockage" class="textButton" placeholder="0%" value="{{old('dockage')}}">
                                        @error('dockage')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="Infestations">Infestations</label>
                                        <input name="infestations" type="text" id="Infestations" class="textButton" placeholder="0%" value="{{old('infestations')}}">
                                        @error('infestations')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Volatile">Volatile</label>
                                        <input name="volatile" type="text" id="Volatile" class="textButton" placeholder="0%" value="{{old('volatile')}}">
                                        @error('volatile')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="hectoliterWeight">Hectoliter Weight</label>
                                        <input name="hectoliter_weight" type="text" id="hectoliterWeight" class="textButton" placeholder="0%" value="{{old('hectoliter_weight')}}">
                                        @error('hectoliter_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Mammalican">Mammalican</label>
                                        <input name="mammalican" type="text" id="Mammalican" class="textButton" placeholder="0%" value="{{old('mammalican')}}">
                                        @error('mammalican')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="ashContent">Ash Content</label>
                                        <input name="ash_content" type="text" id="ashContent" class="textButton" placeholder="0%" value="{{old('ash_content')}}">
                                        @error('ash_content')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="curcuminContent">Curcumin Content</label>
                                        <input name="curcumin_content" type="text" id="curcuminContent" class="textButton" placeholder="0%" value="{{old('curcumin_content')}}">
                                        @error('curcumin_content')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="acidInsolubleAsh">Acid Insoluble Ash</label>
                                        <input name="acid_insoluble_ash" type="text" id="acidInsolubleAsh" class="textButton" placeholder="0%" value="{{old('acid_insoluble_ash')}}" >
                                        @error('acid_insoluble_ash')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="insectDefiled">Insect Defiled/Infested % by Weight</label>
                                        <input name="insect_defiled" type="text" id="insectDefiled" class="textButton" placeholder="0%" value="{{old('insect_defiled')}}">
                                        @error('insect_defiled')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="moldWeight">Mold % by Weight</label>
                                        <input name="mold_by_weight" type="text" id="moldWeight" class="textButton" placeholder="0%" value="{{old('mold_by_weight')}}">
                                        @error('mold_by_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="extraneousWeight">Extraneous % by Weight</label>
                                        <input name="extraneous_by_weight" type="text" id="extraneousWeight" class="textButton" placeholder="0%" value="{{old('extraneous_by_weight')}}">
                                        @error('extraneous_by_weight')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="wholeDeadInsects">Whole Dead Insects</label>
                                        <input name="whole_dead_insects" type="text" id="wholeDeadInsects" class="textButton" placeholder="0%" value="{{old('whole_dead_insects')}}">
                                        @error('whole_dead_insects')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="additional_information">Other option included</label>
                                        <textarea class="textButton form-control" name="additional_information" placeholder="other option included" id="additionalInformation" rows="3">{{ old('additional_information') }}</textarea>
                                        @error('additional_information')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="btn_groups step_one">
                                    <div class="button next_btn">Next Step</div>
                                </div>
                            </fieldset>
                            <fieldset class="section">
                                <h4 class="">Quality Specifications</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="Quantity">Total Quantity <span class="red">*</span></label>
                                        <input name="quantity" type="text" id="Quantity" placeholder="5000" class="textButton" value="{{old('quantity')}}">
                                        @error('quantity')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="input_container">
                                        <label for="preferredUnit">Preferred Volume Unit <span class="red">*</span></label>
                                        <select name="unit_id" id="preferredUnit" class="textButton">
                                          <option value="">Select Unit</option>
                                            @foreach ($units as $unit)
                                            <option value="{{$unit->id}}" @if( old('unit_id')  == $unit->id) selected="selected" @endif>{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="preferredCurrency">Preferred Currency <span class="red">*</span></label>
                                        <select name="currency_id" id="preferredCurrency" class="textButton">
                                          <option value="">Select Currency</option>
                                            @foreach ($currencies as $currency)
                                            <option value="{{$currency->id}}" @if( old('currency_id')  == $currency->id) selected="selected" @endif>{{$currency->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input_container">
                                        <label for="demandPrice">Price per unit <span class="red">*</span></label>
                                        <input name="demand_price" type="number" id="demandPrice" class="textButton" placeholder="6000" value="{{old('demand_price')}}">
                                        @error('demand_price')
                                            <span style="color: #F00;">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="input_group">
                                  <div class="input_container">
                                      <label for="deliveryDate">Delivery Date Deadline <span class="red">*</span></label>
                                      <input type="date" name="delivery_date" id="deliveryDate" class="textButton" value="{{old('delivery_date')}}">
                                      @error('delivery_date')
                                          <span style="color: #F00;">{{$message}}</span>
                                      @enderror
                                  </div>
                                  <div class="input_container">
                                      <label for="preferredLocation">Preferred Delivery Location <span class="red">*</span></label>
                                      <input name="delivery_address" type="text" id="preferredLocation" class="textButton" placeholder="#Address" value="{{old('delivery_address')}}">
                                      @error('delivery_address')
                                          <span style="color: #F00;">{{$message}}</span>
                                      @enderror
                                  </div>
                                </div>
                                <div class="input_group">
                                    <div class="input_container">
                                        <label for="preferredCountry">Preferred Delivery Location Country <span class="red">*</span></label>
                                        <select name="delivery_country_id" id="preferredCountry" class="textButton">
                                          <option value="">Select Country</option>
                                          @foreach ($countries as $country)
                                          <option value="{{$country->id}}" @if( old('delivery_country_id')  == $country->id) selected="selected" @endif>{{$country->name}}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                    <div class="input_container"></div>
                                </div>
                                <h4 class="form_header">Upload image <span style="color: #F00;">*</span></h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="upload_file">
                                    <input type="file" id="uploadFiles" name="file_path" accept="image/*" class="file_upload uploadFiles"  />
                                    <label id="imgUploadLabel" for="uploadFiles">
                                        <span>
                                            <svg class="inline-block mr-1" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M17.8571 2.85706H2.14285C0.95939 2.85706 0 3.81645 0 4.99991V14.9999C0 16.1834 0.95939 17.1427 2.14285 17.1427H17.8571C19.0406 17.1427 20 16.1834 20 14.9999V4.99991C20 3.81645 19.0406 2.85706 17.8571 2.85706ZM18.5714 11.1328L14.0765 6.63775C13.7975 6.3589 13.3454 6.3589 13.0665 6.63775L7.14285 12.5613L4.79071 10.2092C4.5118 9.9303 4.05962 9.9303 3.78071 10.2092L1.42856 12.5613V4.99991C1.42856 4.60541 1.74835 4.28561 2.14285 4.28561H17.8571C18.2516 4.28561 18.5714 4.60541 18.5714 4.99991V11.1328H18.5714Z" fill="#9A9A9A" fill-opacity="0.92"/>
                                                <path d="M6.428 9.99993C7.6115 9.99993 8.57085 9.04053 8.57085 7.85708C8.57085 6.67358 7.6115 5.71423 6.428 5.71423C5.24455 5.71423 4.28516 6.67358 4.28516 7.85708C4.28516 9.04053 5.24455 9.99993 6.428 9.99993Z" fill="#9A9A9A" fill-opacity="0.92"/>
                                            </svg>
                                            Upload image
                                        </span>

                                    </label>
                                    @error('file_path')
                                        <span style="color: #F00;">{{$message}}</span>
                                    @enderror
                                </div>
                                <h4 class="form_header">Agreement</h4>
                                <p style="color: red; font-size: 0.8rem;">All four boxes in this section must be checked to submit the form.</p>
                                <div class="underline"></div>
                                <div class="terms_group">
                                  <div class="checkbox_container">
                                      <input type="checkbox" name="agree_info" id="check1" class="agreement_box" @if( old('agree_info')  == '1') checked @endif value="1">
                                      <label for="check1">
                                          <div class="check_box"
                                          @error('agree_info')
                                          style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                          @enderror>
                                        </div> I agree that all the information I have provided
                                          are correct and true to my best knowledge.
                                      </label>
                                  </div>
                                  <div class="checkbox_container">
                                      <input type="checkbox" name="agree_intention" id="check2" class="agreement_box" @if( old('agree_intention')  == '1') checked @endif value="1">
                                      <label for="check2">
                                          <div class="check_box"
                                          @error('agree_intention')
                                          style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                          @enderror></div> I agree that the company that I am affiliated with
                                          has sincere intention of actually purchasing this products from suitable
                                          suppliers without any
                                          intention to purely check suppliers’ prices or benchmark competitors’ prices
                                          without the intent to purchase.
                                      </label>
                                  </div>
                                  <div class="checkbox_container">
                                      <input type="checkbox" name="agree_feedback" id="check3" class="agreement_box" @if( old('agree_feedback')  == '1') checked @endif value="1">
                                      <label for="check3">
                                          <div class="check_box"
                                          @error('agree_feedback')
                                          style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                          @enderror></div> I agree to provide proper feedback on any offers
                                          proposed to me by any of the suppliers. I also agree to respond to any of the
                                          supplier’s in a
                                          timely manner.
                                      </label>
                                  </div>
                                  <div class="checkbox_container">
                                      <input type="checkbox" name="agree_penalty" id="check4" class="agreement_box" @if( old('agree_penalty')  == '1') checked @endif value="1">
                                      <label for="check4">
                                          <div class="check_box"
                                          @error('agree_penalty')
                                          style="border-style: solid; border-width: thin; border-color: #F00; border-radius: 5px;"
                                          @enderror></div> I understand that, any company can be penalized or
                                          permanently blocked on Farmcrowdy Traders’ platform if any points agreed here
                                          turns out
                                          to be false.
                                      </label>
                                  </div>
                                </div>
                                <div class="btn_groups">
                                    <div class="prev next_btn">Previous Step</div>
                                    <div class="button next_btn">Next Step</div>
                                </div>

                            </fieldset>
                            <fieldset class="section">
                                <h4 class="">Crop Category</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>
                                        <li>Crop :<span id="productIDSpan">Nill</span></li>
                                        <!-- <li>Crop :<span id="cropSpan">Nill</span></li> -->
                                        <li>Color: <span id="colorSpan">Nill</span></li>
                                        <li>Hardness: <span id="hardnessSpan">Nill</span></li>
                                        <li>Crop Type: <span id="cropTypeSpan">Nill</span></li>
                                        <li> Size: <span id="sizeSpan">Nill</span></li>
                                        <li>Drying Process: <span id="DrynessSpan">Nill</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Quality Specifications</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="output_container">
                                  <ul>
                                      <li>Foreign Matter: <span id="MatterSpan">Nill</span></li>
                                      <li> Weevil: <span id="WeevilSpan">Nill</span></li>
                                      <li>Rotten/Shrivelled: <span id="RottenSpan">Nill</span></li>
                                      <li> Splits: <span id="SplitsSpan">Nill</span></li>
                                      <li> Total Defects: <span id="DefectsSpan">Nill</span></li>
                                      <li> Dockage: <span id="DockageSpan">Nill</span></li>
                                      <li> Volatile: <span id="VolatileSpan">Nill</span></li>
                                      <li> Mammalican: <span id="MammalicanSpan">Nill</span></li>
                                      <li> Curcumin Content: <span id="curcuminContentSpan">Nill</span></li>
                                      <li> Insect Defiled/Infested % by Weight: <span id="insectDefiledSpan">Nill</span>
                                      </li>
                                      <li> Extraneous % by Weight: <span id="extraneousWeightSpan">Nill</span></li>
                                      <li> Moisture: <span id="MoistureSpan">Nill</span></li>
                                      <li> Broken Material: <span id="brokenMaterialSpan">Nill</span>
                                      <li>
                                      <li> Damaged Kernel: <span id="damagedKernelSpan">Nill</span></li>
                                      <li> Test Weight: <span id="WeightSpan">Nill</span></li>
                                      <li> Oil Content: <span id="oilContentSpan">Nill</span></li>
                                      <li> Infestations: <span id="InfestationsSpan">Nill</span></li>
                                      <li> Hectoliter Weight: <span id="hectoliterWeightSpan">Nill</span></li>
                                      <li> Ash Content: <span id="ashContentSpan">Nill</span></li>
                                      <li> Acid Insoluble Ash: <span id="acidInsolubleAshSpan">Nill</span></li>
                                      <li> Mold % by Weight: <span id="moldWeightSpan">Nill</span></li>
                                      <li> Whole Dead Insects: <span id="wholeDeadInsectsSpan">Nill</span></li>
                                      <li> Other option included: <span id="additionalInformationSpan">Nill</span></li>
                                  </ul>
                                </div>
                                <h4 class="form_header">Upload image</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>
                                        <li>File: <span id="fileUploadSpan">Nill</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Quantity Specification</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="output_container">
                                    <ul>

                                        <li>Quantity: <span id="QuantitySpan">Nill</span></li>
                                        <li>Preferred Currency: <span id="preferredCurrencySpan">Nill</span></li>
                                        <li> Preferred Delivery Location: <span id="preferredLocationSpan">Nill</span></li>
                                        <li> Preferred Volume Unit: <span id="preferredUnitSpan">Nill</span></li>
                                        <li> Price per unit: <span id="demandPriceSpan">Nill</span></li>
                                        <li> Delivery Date Deadline: <span id="deliveryDateSpan">Nill</span></li>
                                        <li> Preferred Delivery Location Country: <span
                                                id="preferredCountrySpan">Nill</span></li>
                                    </ul>
                                </div>
                                <h4 class="form_header">Agreement</h4>
                                {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> --}}
                                <div class="underline"></div>
                                <div class="output_container terms_preview">
                                    <ul>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I agree that all the information i have provided are correct and true to my best
                                            knowledge.
                                        </li>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I agree that the company that I am affiliated with has sincere intention of actually purchasing this products from suitable suppliers without any
                                            intention to purely check suppliers’ prices or benchmark competitors’ prices without the intent to purchase.
                                        </li>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I agree to provide proper feedback on any offers proposed to me by any of the suppliers. I also agree to respond to any of the supplier’s in a timely manner.
                                        </li>
                                        <li><svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <rect x="0.5" y="0.5" width="23" height="23" rx="4.5" stroke="#A9B2B2" />
                                                <path d="M5.5 12.1818L8.42958 16L18.5 8" stroke="#A9B2B2" stroke-width="2"
                                                    stroke-linecap="round" stroke-linejoin="round" />
                                            </svg>
                                            I understand that, any company can be penalized or permanently blocked on Farmcrowdy Traders’ platform if any points agreed here turns out to be false.
                                        </li>

                                    </ul>
                                </div>
                                <div class="btn_groups">
                                    <div class="prev next_btn">Previous Step</div>
                                    <button type="submit" class="next_btn">Submit</button>
                                    <!-- <div class="button next_btn">Next Step</div> -->
                                </div>

                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

  @endsection
  @section('scripts')
  <script src="{{ asset('js/fileUploader.min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  @endsection('scripts')
