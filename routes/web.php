<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes();
Route::get('/logout','\App\Http\Controllers\Auth\LoginController@logout');

//gmail login
Route::get('/login/google', 'Auth\LoginController@redirectToGoogle')->name('google');
Route::get('/login/google/callback', 'Auth\LoginController@handleGoogleCallback')->name('handle-google');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/orders', 'HomeController@orders')->name('orders');
Route::get('/create_order', 'HomeController@create_order')->name('create_order');
Route::get('/edit_order/{id}', 'HomeController@edit_order')->name('edit_order');
Route::get('/order_details', 'HomeController@order_details')->name('order_details');
Route::get('/order_details/{id}', 'HomeController@order_details');

Route::get('/offers', 'HomeController@offers')->name('offers');
Route::get('/create_offer', 'HomeController@create_offer')->name('create_offer');
Route::get('/edit_offer/{id}', 'HomeController@edit_offer')->name('edit_offer');
Route::get('/offer_details/{id}', 'HomeController@offer_details')->name('offer_details');

Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/my_listings', 'HomeController@my_listings')->name('my_listings');
Route::post('/update_profile', 'UserProfileController@update')->name('update_profile');
Route::post('/send_message', 'MessageController@store')->name('send_message');
Route::post('/load_prev_messages', 'MessageController@previous_messages')->name('load_prev_messages');
Route::post('/load_recent_messages', 'MessageController@recent_messages')->name('load_recent_messages');
Route::post('/load_latest_messages', 'MessageController@latest_messages')->name('load_recent_messages');
Route::get('/insights', 'HomeController@insights')->name('insights');
Route::get('/search_product','HomeController@search_product')->name('search_product');
Route::get('/search_offer','HomeController@search_offer')->name('search_offer');

Route::middleware('auth')->group(function() {
    Route::post('/new_order', 'OrderController@create')->name('new_order');
    Route::post('/update_order', 'OrderController@update')->name('update_order');
    Route::get('/clone_order/{id}', 'OrderController@clone')->name('clone_order');
    Route::get('/delete_order/{id}', 'OrderController@delete')->name('delete_order');

    Route::post('/new_offer', 'OfferController@create')->name('new_offer');
    Route::post('/update_offer', 'OfferController@update')->name('update_offer');
    Route::get('/clone_offer/{id}', 'OfferController@clone')->name('clone_offer');
    Route::get('/delete_offer/{id}', 'OfferController@delete')->name('delete_offer');

});
